import React, { Component } from 'react';
import { Button, View ,Text,Container,Content,Header,StyleProvider} from 'native-base';
import { AppLoading } from 'expo';
import * as Font  from 'expo-font';
import { Ionicons,MaterialCommunityIcons,MaterialIcons,Entypo,FontAwesome } from '@expo/vector-icons';
import { Provider } from "react-redux";

import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import { createAppContainer } from 'react-navigation';
import AppNavigator from"./navigation/AppContainer.js";
const AppContainer = createAppContainer(AppNavigator);
import config from "./store/config";

export default class App extends  Component {

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
      ...MaterialCommunityIcons.font,
      ...MaterialIcons.font,
      ...Entypo.font,
      ...FontAwesome.font
    });
    this.setState({ isReady: true });
  }
  render() {
    const store = config();
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <Provider store = { store } >
      <StyleProvider style={getTheme(material)}>
      <AppContainer/>
      </StyleProvider>
      </Provider>
    );
  }
}
