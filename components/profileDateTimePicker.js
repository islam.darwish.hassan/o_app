import React, {useState} from 'react';
import {View, Button, Platform,TouchableOpacity,Text,Dimensions} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';


const App = (props) => {
  const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width;
   
   const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  if(props.date!=""||props.date!=null){
    console.log("date from parent : "+props.date);
    //this.props.dateChange(this.props.date);
  }


  const onChange = (event, selectedDate) => {
      if (selectedDate!==""&&selectedDate!==null && selectedDate !== undefined )
     {
         const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    let date = currentDate;
        console.log("iam here "+date);

    let year=    currentDate.getFullYear();
    let month=    currentDate.getMonth() + 1;
    let day=    currentDate.getDate();
    console.log(year+"-"+month+"-"+day)
    props.dateChange(year+"-"+month+"-"+day);
}  else { setShow(false) }
    
   }
 
     
 
  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };
  
  const hideDatepicker = ()=> {
    setShow(false)  
  }
  const showTimepicker = () => {
    showMode('time');
  };

 
  return (
    <View style={{flex:1}}>
      <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
      <Text style={{fontSize:12}} >Birth  :</Text>
         <Text style={{backgroundColor:'#fff',flex:0.5,fontSize:16,textAlign:'right',color:'#000',paddingRight:6}}
          onPress={show == false ? showDatepicker : hideDatepicker }>
         {`${props.date ?   props.date     : "Birth Date" }`}  
         </Text> 
        
      </View>
     
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          minimumDate={new Date(1920, 1, 1)}
          maximumDate={new Date()}
          value={date}
          locale={"en"}
          mode={mode}
          style={{ width:deviceWidth,alignSelf:"center",flex:1}}
          onChange={onChange}
          
        />
      )}
    </View>
  );
};

export default App;