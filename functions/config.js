export const API ="https://oappweb.com/api/";
export const APIV="v1/";

// login request & reg
export const Login ="auth/login";
export const Sign="auth/register";
export const Governs="general/locations/countries/66/governrates";
export const City1="general/locations/governrates/";
export const City2="/cities"

//Home page

export const Home="home";
export const Store="stores/" //+store_id /products
export const Categories="categories"
export const Product="products/"
export const Locate="profile/locate"
export const Nearest_Stores="stores/nearest"
export const Profile="profile";
export const Orders="profile/orders" // ?page=1&status=0
export const Discounts="discounts?page=1"
export const Calculate="orders/calculate";
export const Checkout="orders/checkout";
export const FAKE_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJkNDg3NmJjMzUzNWZiNjIzNTliNjcwNTk0NDE4Mzk2NGQ0MTc3MzFiNzQ0NzI5YmNmZWFjYzJiNzE3ZWVhMzA0NTk1MGE0ODc1Njk3MDE4In0.eyJhdWQiOiI1IiwianRpIjoiYmQ0ODc2YmMzNTM1ZmI2MjM1OWI2NzA1OTQ0MTgzOTY0ZDQxNzczMWI3NDQ3MjliY2ZlYWNjMmI3MTdlZWEzMDQ1OTUwYTQ4NzU2OTcwMTgiLCJpYXQiOjE1ODk5MjY2NjIsIm5iZiI6MTU4OTkyNjY2MiwiZXhwIjoxNjIxNDYyNjYyLCJzdWIiOiI2NiIsInNjb3BlcyI6W119.bpccAdzIM-MzP0DsY6BGKfApscQa--EDQolBd_5e4BFDYyJv3bkR0kydL8Pi7fvpQUmI0J_NAA5Ar_uI4oxj3R_mfgK_wxnDO0RRyWfJLgS9rngF6OvuJONWsM2RiTZ9-1aoE-fFnG0v0aCm2vNvOQbX411OFBAAnf-uHD4imo95V-30r4esZ_22IYPmoU7lPxpvD_jW1mr_6dtq5ZSOpfABqevmOaF3KfkdPHNzOKUccAgo3FLrSBoFQJPYHg7hlVZWs6oUJ7enKxN0bUKt1U_zZR-tG-0wzwdhvUNTDD39RJlIbkTaGPaxkJHD71pz7L60YJbD4k0VasHyph7fBH4koy7LDmuTp4zV2i8vt996lrlxeg6-_BKorl8P6E4ANIJX9q4tkaSrP3M2fSmI0dZW4VnBXdFWdAqdKUaTzqu_JgvGhUAJRjnUJLSIwbyVVaRP7KbHlxfdBPwyiA0vL5nAj5iNxDHrTA24HQ9neXoHUskLhP8MyfdjtRJzBeqpHLzKd4X5crBFIaYxCnnCZPOCX2hIWFkWsZ9-NJw0pyQRliC_ONRqZthrxEaaVHgHou5-9Og6r6GgP6m9Xk3tJ4Zf-VQdxSmMN2OdYGqyU8FeNfMNJebdpzmXHAbFpu8ydOp72ItHnR5d94mWrSkYNpiMkLgY1Wpz253aHL0_-eQ"