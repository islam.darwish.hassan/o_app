import { ADD_ITEM, DELETE_ITEM, SELECT_ITEM, DESELECT_ITEM, SET_CART_ITEMS} from "../actions/actionTypes";
let initial_state = {
  items: [],
  selectedItem: null,
}
const cart= (state=initial_state,action)=>{

    switch (action.type) {
      case ADD_ITEM:
          if(state.items.find((value,index) => { return value.id === action.id})!=null)
          {
            state.items.filter((value,index) => { if(value.id === action.id) {return value.qty = action.qty+value.qty}else{return value.qty =value.qty }})
          }
        return {
          ...state,
          items: state.items.find((value,index) => { return value.id === action.id})==null ?
          state.items.concat({
            id:action.id,
            description:action.description,
            qty: action.qty,
            image:action.image,
            name:action.name,
            note: action.note,
            price:action.price,
            discount:action.discount,
            rate:action.rate,
            })
          :  
          state.items
          
        };
      case DELETE_ITEM:
        return {
          ...state,
          items: state.items.filter(item => {
            return item.id !== action.id;
          }),
          selectedItem: null
        };
      case SELECT_ITEM:
        return {
          ...state,
          selectedItem: state.items.find(item => {
              return item.id === action.id;
          })
        };
      case DESELECT_ITEM:
        return {
          ...state,
          selectedItem: null
        };
      case SET_CART_ITEMS:
        return {
          ...state,
          items:action.items
        };

      default:
        return state;
    }
}
export default cart;
