import {ADD_ITEM, DELETE_ITEM, SELECT_ITEM, DESELECT_ITEM, SET_CART_ITEMS } from "./actionTypes";

export const addItem = (item) => {
    return {
        type: ADD_ITEM,
        //set item data
        id:item.id,
        qty:item.qty,
        note:item.note,
        description:item.description,
        image:item.image,
        name:item.name,
        price:item.price,
        discount:item.discount,
        rate:item.rate,

}
};
export const deleteItem = (item) => {
    return {
        type: DELETE_ITEM,
        // id to select item , qty to delete 
        id:item.id
    }
};
export const selectItem = (id) => {
    return {
        type: SELECT_ITEM,
        id: id
    }
};

export const deselectItem = () => {
    return {
        type: DESELECT_ITEM,


    }
};
export const setItems = (items) => {
    return {
        type: SET_CART_ITEMS,
        items: items,
    };

}