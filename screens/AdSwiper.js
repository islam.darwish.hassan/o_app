import React, { Component } from "react";

import {
    Container,
    Header,
    CheckBox,
    Content,
    Button,
    Icon,
    Left,
    Right,
    Body,
    Text,
    Form,
    Item,
    Label,
    Input,
    View,
    Footer,
} from "native-base";
import { ImageBackground ,StatusBar  } from "react-native";
import { AsyncStorage ,  Dimensions, Image,
     } from "react-native"
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet,TouchableOpacity } from 'react-native';

import Swiper from 'react-native-swiper';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;
import ImageViewer from 'react-native-image-zoom-viewer';
import { Modal } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';

const styles = StyleSheet.create({
    wrapper: {
        flex:1
    },
    slide1: { 
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    },
    text: {
      color: '#25D366',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  const images = [{
    // Simplest usage.
    url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',
 
    // width: number
    // height: number
    // Optional, if you know the image size, you can set the optimization performance
 
    // You can pass props to <Image />.
    props: {
        // headers: ...
    }
}]


export default class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkterms:true,
      visible:false
    }}
    render() {
      var width=this.props.width?this.props.width:deviceWidth;
      var height=this.props.height?this.props.height:deviceHeight/3;
        return (
          this.props.slides?

                    <View style={{height:height}}>
                    <Modal  visible={this.state.visible} transparent={true}>
                    <ImageViewer renderHeader	={(currentIndex) => (

                      <View style={{alignSelf:"flex-end",paddingHorizontal:15,paddingVertical:8}}>
                      <TouchableOpacity onPress={()=>this.setState({visible:false})}>
                      <Text style={{color:"#fff" ,fontWeight:"bold"}}>Cancel</Text>
                      </TouchableOpacity>
                      </View>)} 
                      enableSwipeDown={true}	 
                      onCancel={()=>this.setState({visible:false})} 
                      imageUrls={this.props.slides.map((value)=>{
                     swipeDownThreshold={height}
                        if(value.image){
                         return {url:this.props.link+value.image}
                        }
                        else if(value.sub_photo){
                          return {url:this.props.link+value.sub_photo}
                        }else{
                          return {url:this.props.link+value.src}

                        }
                      })}/>
               
                      </Modal>
                      <SwiperFlatList 
                        
                        autoplay
                        autoplayDelay={2}
                        autoplayLoop
                        index={0}
                        >
                    {this.props.slides.map((value,index)=>{
                        return(
                        <View key={index} style={[styles.slide1,{flex:1,backgroundColor:"transparent"}]}>
                          {value.image?
                            <TouchableOpacity onPress={()=>this.setState({visible:true})}>
                           <Image source={{uri:this.props.link+value.image}} style={{resizeMode:this.props.mode?this.props.mode:"contain",height,width}}/> 
                           </TouchableOpacity>

                           :
                           value.sub_photo ?
                           <TouchableOpacity onPress={()=>this.setState({visible:true})}>
                          <Image source={{uri:this.props.link+value.sub_photo}} style={{resizeMode:this.props.mode?this.props.mode:"contain",height,width}}/> 
                          </TouchableOpacity>
                          : 
                          <TouchableOpacity onPress={()=>this.setState({visible:true})}>
                         <Image source={{uri:this.props.link+value.src}} style={{resizeMode:this.props.mode?this.props.mode:"contain",height,width}}/> 
                         </TouchableOpacity>


                        }
                         {/* <Text style={styles.text}>{this.props.link+value.photo}</Text>  */}
                        </View>
                   )}
                   )}
                            </SwiperFlatList>
                  </View>
                
                :null
              );
    }
}

 
