import React, { Component } from 'react'
import { Image, Linking, Dimensions, StyleSheet, ScrollView, TouchableOpacity, FlatList, TextInput } from "react-native";
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left, Body, Thumbnail
} from 'native-base';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
 import styles from "../screens/style";
 
 
 import { FirebaseRecaptchaVerifierModal } from "expo-firebase-recaptcha";
import * as firebase from "firebase";
import { Alert } from "react-native";





// PROVIDE VALID FIREBASE CONFIG HERE
// https://firebase.google.com/docs/web/setup
const FIREBASE_CONFIG: any = {
    apiKey: "AIzaSyBJ9yHlcpxpBZaby2d6oT5r6-zvYO6bVbc",
    authDomain: "o-app-5fdfd.firebaseapp.com",
    databaseURL: "https://o-app-5fdfd.firebaseio.com",
    projectId: "o-app-5fdfd",
    storageBucket: "o-app-5fdfd.appspot.com",
    messagingSenderId: "1082208321868",
    appId: "1:1082208321868:web:44feb8de78502902258fe9"
  };


try {
  if (FIREBASE_CONFIG.apiKey) {
    firebase.initializeApp(FIREBASE_CONFIG);

  }
} catch (err) {
  // ignore app already initialized error on snack
}




export default function VerifyPhoneReg(props) {



 
  const [counter, setCounter] = React.useState(0);
  let [isReady, setIsReady] = React.useState(true)
  const recaptchaVerifier = React.useRef(null);                       // Recaptcha
  const [phoneNumber, setPhoneNumber] = React.useState();
  const [verificationId, setVerificationId] = React.useState();       // verification id 
  const [verificationCode, setVerificationCode] = React.useState();   // verification code 
  const firebaseConfig = firebase.apps.length ? firebase.app().options : undefined;    // Recaptcha 
  const [message, showMessage] = React.useState((!firebaseConfig || Platform.OS === 'web')
    ? { text: "To get started, provide a valid firebase config in App.js and open this snack on an iOS or Android device." }
    : undefined);




  const naviagte_next = async () => {


    let data = {
     
      phone: phoneNumber
    }
    await setCounter(counter = 0);
        props.navigation.navigate("SignUp", { data: data })

  }



  return (
    isReady ?
      <View style={{ height: deviceHeight, width: deviceWidth, backgroundColor: '#FF952B', alignSelf: 'center', alignContent: 'center', paddingTop: 10 }}>
        <FirebaseRecaptchaVerifierModal            // Modal of the Recaptcha Verifier // 
          ref={recaptchaVerifier}
          firebaseConfig={firebaseConfig}
        />
        <View padder style={{ marginTop: 100,alignItems:'flex-start'  }}>
          {counter == 1 ? 
                  <Icon onPress={() =>  setCounter(counter - 1)} style={{ color: "#fff",fontSize:30,  marginHorizontal:20 }} type="MaterialCommunityIcons" name="arrow-left"  />

          : null }
        </View>
        
        {counter == 0 ? <View style={{ flex: 1, alignContent: 'center',alignItems:'center', justifyContent:'flex-start' }}>
             <View style={{justifyContent:'center',marginTop:40,marginBottom:10}}>
        <Icon   style={{ color: "#fff",  fontSize:150 ,margin:0}} type="MaterialIcons" name="phone-android"   />
        {/* <Icon   style={{ color: "#fff",  fontSize:90}} type="MaterialIcons" name="verified-user"   /> */}
         
              </View>
              <Text style={{ fontSize: 27,color:'#fff', fontWeight: 'bold',marginVertical:10, alignSelf: 'center' }}>PHONE VERIFICATION</Text>

          <View style={{ flexDirection: 'row',alignSelf:'center',}}>
            <Text
              style={[styles.ButtonRoundedColored,{width:50,textAlignVertical:'center',textAlign:'center',justifyContent:'center'}, styles.BorderColorYellow, styles.BgWhite]} >+2</Text>
            <Item regular

              style={[styles.ButtonRoundedColored,{width:250,borderColor:"#fff"}, styles.BorderColorYellow, styles.BgWhite]}
            //error={this.state.EmailError}
            >
              <Input style={[styles.TextBold]}
                placeholder={"Enter phone number"}
                placeholderTextColor={"#707070"}
                onChangeText={(phoneNumber) => setPhoneNumber('+2'.concat(phoneNumber))}
                keyboardType="phone-pad"
              />

            </Item>
          </View>



          <TouchableOpacity
            title="Send Verification Code"
            disabled={!phoneNumber}
            style={[styles.ButtonRoundedColored, styles.BgYellow,]}
            onPress={async () => {

              // The FirebaseRecaptchaVerifierModal ref implements the
              // FirebaseAuthApplicationVerifier interface and can be
              // passed directly to `verifyPhoneNumber`.
              try {
                const phoneProvider = new firebase.auth.PhoneAuthProvider();
                const verificationId = await phoneProvider.verifyPhoneNumber(
                  phoneNumber,
                  recaptchaVerifier.current
                );
                setVerificationId(verificationId);
                // showMessage({
                //   text: "Verification code has been sent to your phone.",
                // });
                alert("Verification code has been sent to your phone.");
                await setIsReady(isReady = false)
                await setCounter(counter + 1);
                console.log(counter)
                await setIsReady(isReady = true)

              } catch (err) {
                alert(err.message);
                console.log(err.message)
              }
            }}
          ><Text style={[styles.TextBold,{color:'#FF952B',fontSize:16}]}>{"Send verification code"}</Text>
          </TouchableOpacity>





        </View>
          : counter == 1 ?
          <View style={{ flex: 1, alignContent: 'center',alignItems:'center', justifyContent:'flex-start' }}>
          <View style={{justifyContent:'center',marginTop:40,marginBottom:10}}>
      <Icon   style={{ color: "#fff",  fontSize:150 ,margin:0}}  type="MaterialIcons" name="verified-user"   />
      
           </View>
           <Text style={{ fontSize: 27,color:'#fff', fontWeight: 'bold',marginVertical:10, alignSelf: 'center' }}>PHONE VERIFICATION</Text>

 

              <Item regular
                style={[styles.ButtonRoundedColored, styles.BorderColorYellow, styles.BgWhite]}
              //error={this.state.EmailError}
              >
                <Input style={[styles.TextBold]}
                  placeholder={"Enter code"}
                  placeholderTextColor={"#707070"}
                  onChangeText={setVerificationCode}
                  keyboardType="phone-pad"
                />

              </Item>





              <TouchableOpacity
                title="Confirm Verification Code"
                disabled={!verificationId}
                style={[styles.ButtonRoundedColored, styles.BgYellow,]}

                onPress={async () => {
                  try {
                    const credential = firebase.auth.PhoneAuthProvider.credential(
                      verificationId,
                      verificationCode
                    );
                    await firebase.auth().signInWithCredential(credential);
                    alert("Phone authentication successful 👍");
                    naviagte_next()

                  } catch (err) {
                    alert(err.message);
 
                   }
 
                }}
              ><Text style={[styles.TextBold],{color:'#FF952B'}}>{"Send verification code"}</Text></TouchableOpacity>
              {message ? (
                <TouchableOpacity
                  style={[StyleSheet.absoluteFill, { backgroundColor: 0xffffffee, justifyContent: "center" }]}
                  onPress={() => showMessage(undefined)}>
                  <Text style={{ color: message.color || "blue", fontSize: 17, textAlign: "center", margin: 20, }}>
                    {message.text}
                  </Text>
                </TouchableOpacity>
              ) : undefined}



            </View>

            : null}


      </View> : null
  );
}
