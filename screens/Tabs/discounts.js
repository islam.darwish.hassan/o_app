import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text,
  Button, View,List, Right, Footer, ListItem,Left,
  Body, 
  Thumbnail } from 'native-base';
import { API, APIV, Discounts } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image, ActivityIndicator, ScrollView, SafeAreaView,
  Dimensions,FlatList, PanResponder, Animated, TextInput } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from"../Loader";
import StoreMan from "../../functions/storage";
import SmallLoader from '../pages/SmallLoader';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
export default class DiscountsPage extends Component {
    //construction
    constructor(props) {
      super(props);
      this.Storager = new StoreMan();
  };
  
    state={token:null,data: [] ,isReady:false,products:[], refreshing: false,page:2,

    fetching_from_server: false,endThreshold: 2 ,enablescroll: true,
    
    }
   
   
   
    componentDidMount=async()=>{
      await this.setState({ isReady: false });
        const { navigation } = this.props;
         const Data = navigation.getParam('data', '');
        await this.setState({data:Data});
        const stored_token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({token:stored_token})
        await this.Get_Discounts('reset');
        await this.setState({isReady:true});

    }

    
  Get_Discounts = async (mode) => {
    this.setState({ fetching_from_server: true });
    const tokens = this.state.token;
    if (mode == 'reset') {
      await axios
        .get(API + APIV + "discounts" + "?page=1" 
          , {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": " Bearer " + tokens
            },
          })
        .then(result => {
      
            this.setState({
              data: result.data.data.discount_products,
              page: 2 //next page,
              
            })
 
        })
        .catch(error => {
          //console.log(error);
          if (error.response) {
            // error handling
            // alert(error.response.data.error);
          }
        });
    } else {
      console.log('get data of page -> ' + this.state.page)
      await axios
        .get(API + APIV + 'discounts' +  "?page=" + this.state.page 
          , {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": " Bearer " + tokens
            },

          })
        .then(result => {
          console.log(result.data.data.discount_products.length)

            if (result.data.data.discount_products.length > 0) {
              console.log("iamhere")
              this.setState({ data: [...this.state.data, ...result.data.data.discount_products] })
              this.setState({ page: this.state.page + 1 })

            } else {
              console.log("endreached")

            }


        })
        .catch(error => {
          // console.log(error.response.data.error);
        });


    }
    console.log(this.state.data.length);
     this.setState({ fetching_from_server: false });

  }

  
  

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.Get_Discounts('reset').then(() => {
      this.setState({refreshing: false});
    });
  }   

  
  render() {
    return (
      this.state.isReady ? 
      <SafeAreaView >
        <View>
          <View  padder style={{flexDirection:"row" , justifyContent:"space-between" ,alignItems:"center"}}>
        <Text note  >  Products</Text>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Dis")}>
        <Icon style={{color:"#111"}} type="MaterialIcons" name="search" />
        </TouchableOpacity>
        </View>
        </View>
        
        
 
<SafeAreaView style={{  backgroundColor: "#fff", borderTopWidth: 1,
 borderTopColor: "#ededed", marginTop: 5 }}>
  {this.state.data.length > 0 ?
    <FlatList
      scrollEnabled={this.state.enablescroll}
   
      data={this.state.data}
       ListFooterComponent={<View/>}
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh.bind(this)} 
        nestedScrollEnabled = {true}
        scrollIndicatorInsets
      
      onEndReachedThreshold={this.state.endThreshold}
      onEndReached={() => {
        console.log("loadmore")
        {
          !this.state.fetching_from_server ?
          this.Get_Discounts('scroll')
          : null
        }
      }}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index, separators }) => {
        let value = item;  
         return (
           <View key={index} data={value} >
            <ListItem   onPress={()=>this.props.navigation.navigate("Product",{
                data:value
              })}>    
              <Left style={{flex:0.3}}>
              <Thumbnail large resizeMethod='auto' resizeMode="contain"
              source={{ uri: value.image }} >
              </Thumbnail>
              </Left >
                <Body style={{flex:0.8}}>
                  <View >
                    <Text numberOfLines={2}style={[styles.TextList],{width:"95%",fontWeight:'bold' }}>{value.name}</Text>
                    <Text numberOfLines={2} note>{value.description}</Text>
                  </View>
                </Body>
                {value.discount.has_discount?
                <View style={styles.Offerbadge}><Text style={styles.TextSmallWhite}>{value.discount.value}</Text></View>
                :null}
              </ListItem>
           </View>
        )
      }}
    />
    :
    <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
      <Text style={styles.TextBrand}> There is no data available ! 😴</Text>
    </View>}

</SafeAreaView>
{this.state.fetching_from_server ? (
  <SmallLoader/>
) : null}
</SafeAreaView>
   : <Loader/>
  
    )
  }
}
