import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left,
  Body,
  Thumbnail
} from 'native-base';
//functions
import StoreMan from "../../functions/storage";
//axios 
import axios from "axios";
import { API, APIV, Calculate } from "../../functions/config";

import { TouchableOpacity, Image, TouchableWithoutFeedback, ScrollView } from "react-native";
import { Dimensions } from "react-native";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import styles from "../style";
import Loader from "../Loader";
//redux 
import { connect } from "react-redux";
import { addItem, deleteItem, setItems } from "../../store/actions/index";

class Cart extends Component {
  //construction
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };
  state = { token: null, data: null, isReady: false, products: [] }
  componentDidMount = async () => {
    this.setState({ isReady: false });
    const token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({ products: this.props.RX_items, token });
    await this.calculate_price();
    this.setState({ isReady: true });
  }
  calculate_price = async () => {
    let prices = 0;
    this.props.RX_items.map(value => { return (prices += value.price * value.qty) })
    await this.setState({ prices: prices });
    console.log(prices)
  }
  RemoveFromCart = async (value) => {
    await this.props.OnRemove(value);
    await this.setState({ products: this.props.RX_items });
    await this.calculate_price();
  }
  Checkout_Cal = async () => {
    this.setState({ isReady: false });
    const token = this.state.token;
    var bodyParameters = {
      "note": "hat el order bsr3a",
      products: this.props.RX_items,

    }
    var config = {

      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        'Authorization': " Bearer " + token
      }
    }
    //here you are login request 
    await axios
      .post(API + APIV + Calculate, bodyParameters,
        config)
      .then(result => {
        console.log("Order has been calculated");
        this.props.navigation.navigate("Checkout", { data: result.data.data.order_details })
      })
      .catch(function (error) {
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({ isReady: true });

  }
  render() {
    return (

      this.state.isReady ?
        <View style={{ height: null, backgroundColor: "#F1F2F2" }}>


          <View style={{
            height: null, shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4,
          }}>
            <Content style={{ height: null, borderBottomWidth: 0.5, borderColor: '#CDCDCD', }} >

              {this.props.RX_items.length > 0 ?

                this.props.RX_items.map((value, index) => {
                  let product = value
                  return (
                    <List
                      style={{ marginBottom: 10, backgroundColor: '#fff' }}
                      key={index}>
                      <ListItem onPress={() => this.props.navigation.navigate("Product", {
                        data: product
                      })}>
                        <Left style={{ flex: 0.3 }}>
                          <Thumbnail large resizeMethod='auto' resizeMode="contain"
                            source={{ uri: product.image }} >
                          </Thumbnail>
                        </Left >
                        <Body style={{ flex: 0.8 }}>
                          <View >
                            <Text numberOfLines={2} style={[styles.TextList],{width:"95%",fontWeight:'bold' }}>{product.name}</Text>
                            <Text numberOfLines={2} note>{product.description}</Text>
                          </View>
                        </Body>
                        {product.discount.has_discount ?
                          <View style={styles.Offerbadge}><Text style={styles.TextSmallWhite}>{product.discount.value}</Text></View>
                          : null}
                      </ListItem>
                      <ListItem style={{ justifyContent: "space-between", paddingHorizontal: 10 }}>
                        <Text><Text note >Price: </Text>{value.price}</Text>
                        <Text><Text note >Quantity: </Text>{value.qty}</Text>
                      </ListItem>
                      <ListItem onPress={() => this.RemoveFromCart(value)} style={{ justifyContent: "center", paddingHorizontal: 10 }}>
                        <Text note>Remove this item </Text>
                      </ListItem>
                        
                      
                    </List>

                  );

                }
                ) :
                <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                  <Text style={styles.TitleSmallWhite}> Your Cart is Empty ! 😴</Text>
                </View>
              }
<View style={{ backgroundColor: '#fff' }}>
            {this.props.RX_items.length > 0 ?
              <ListItem noBorder style={{ justifyContent: 'flex-start', paddingHorizontal: 2, flexDirection: 'column', alignContent: 'flex-start' }}>
                <Text style={{ fontWeight: 'bold', alignSelf: 'center', marginTop: 3 }} >Total Price : <Text style={{ fontWeight: 'bold', color: '#000' }} note >  {this.state.prices} EGP</Text></Text>

              </ListItem>

              : null}
            {this.props.RX_items.length > 0 ?
              <TouchableOpacity style={[styles.CartButton, { marginBottom: 30 }]} onPress={() => this.Checkout_Cal()} >
                <Text style={[styles.CardButtonText]}>Proceed to checkout</Text>
              </TouchableOpacity>
              : null}
          </View>
            </Content>  
          </View>
          
        </View>
        : <Loader />

    )
  }
}
const mapStateToProps = state => {
  return {
    RX_items: state.cart.items,
    RX_selectedItem: state.cart.selectedItem,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    OnAdd: item => { dispatch(addItem(item)) },
    OnRemove: item => { dispatch(deleteItem(item)) },
    OnSetAll: items => { dispatch(setItems(items)) }
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);
