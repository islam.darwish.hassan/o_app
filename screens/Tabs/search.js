import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text,
   Button, View,List, Right, Footer, ListItem,Left,
   Body, 
   Thumbnail } from 'native-base';
import axios from "axios";
import { Image, ActivityIndicator, ScrollView, SafeAreaView, Dimensions, PanResponder, Animated, TextInput } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity, } from 'react-native-gesture-handler';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { API, APIV, Stores, faketoken } from "../../functions/config";
import Loader from "../Loader";
import StoreMan from "../../functions/storage";
import MapView from 'react-native-maps';
import styles from "../style";
import SmallLoader from '../pages/SmallLoader';



export default class search extends Component {

  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };



  state = {
    deviceHeight: Dimensions.get('window').height,
    enablescroll: true,
    token: null, data: [], isReady: false, page: 2,
    fetching_from_server: false, search_text: "",
    endThreshold: 2, showMap: false,
    showPrices: false,
    //filters
    pricing_class: "",
    verified: "",
    subscribed: "",
    filterBy: "",
    is_opened: "",
    name: "",
    tagName: "",
    tagsMode: false,
    address: "",
    nearest: "", refreshing: false, tags: []
  }




  componentDidMount = async () => {
    await this.setState({ isReady: false });
    const { navigation } = this.props;
    const Data = navigation.getParam('data', '');
    // let focusOnSearch = navigation.getParam('focus',false);
    //  let focusOnSearch = route.params.focus ?? false ;
    // const toggleModel=navigation.getParam('toggle',false);
    //   await this.setState({ sub_category_data: Data ,toggleModel,focusOnSearch:focusOnSearch})
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({ token: stored_token });
      await this.Get_Stores('reset');
    await this.setState({ isReady: true });

  }



  Get_Stores = async (mode) => {
    this.setState({ fetching_from_server: true });
    const tokens = this.state.token;
    if (mode == 'reset') {
      await axios
        .get(API + APIV + "stores/" + "search" + "?page=1&name=" + this.state.name
          , {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": " Bearer " + tokens
            },
          })
        .then(result => {
          if (this.state.filterBy == "nearest") {
            this.setState({
              data: result.data.data,
              page: 2 //next page

            })


          } else {
            this.setState({
              data: result.data.data.stores,
              page: 2 //next page

            })

          }
        })
        .catch(error => {
          //console.log(error);
          if (error.response) {
            // error handling
            // alert(error.response.data.error);
          }
        });
    } else {
      console.log('get data of page -> ' + this.state.page)
      await axios
        .get(API + APIV + 'stores/' + "search" + "?page=" + this.state.page + "&name=" + this.state.name
          , {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": " Bearer " + tokens
            },

          })
        .then(result => {
          console.log(result.data.data.stores.length)

          if (this.state.filterBy == "nearest") {
            if (result.data.data.length > 0) {
              this.setState({ data: [...this.state.data, ...result.data.data] })
              this.setState({ page: this.state.page + 1 })
              console.log("iam here")
            } else {
              console.log("endreached")
            }


          } else {
            if (result.data.data.stores.length > 0) {
              console.log("iamhere")
              this.setState({ data: [...this.state.data, ...result.data.data.stores] })
              this.setState({ page: this.state.page + 1 })

            } else {
              console.log("endreached")

            }

          }

        })
        .catch(error => {
          // console.log(error.response.data.error);
        });


    }
    console.log(this.state.data.length);
    console.log(this.state.data);
    this.setState({ fetching_from_server: false });

  }

  setSearchText = (text) => {
    var self = this;
    self.setState({ name: text })
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.Get_Stores('reset').then(() => {
      this.setState({refreshing: false});
    });
  }


  render() {
    return (
      this.state.isReady ?
        <View>
          <View style={{ height: 65,width:deviceWidth, backgroundColor: "#FF952B", borderTopWidth: 1, borderColor: '#fff',alignContent:"center"
            ,alignSelf:'center',justifyContent:'center' }}
            >
            

              <View  style={{ backgroundColor: "#fff", padding: 10, borderRadius: 15,alignSelf:'center' }} >

                <View style={{
                  flexDirection: "row", width: deviceWidth - 100, height: 30, alignContent: 'center',
                  justifyContent: 'center', alignItems: 'center'
                }}>
                  {this.state.name == "" ?
                    <Icon style={{ color: "#111", alignSelf: 'center' }} type="MaterialIcons" name="search" />
                    : null}
                  <TextInput style={{
                    height: 40, borderBottomWidth: 1, borderColor: "#ededed", width: (deviceWidth - 40) / 1.35
                    , paddingLeft: 8
                  }}
                    placeholder={" Search ... "}
                    onChangeText={text => this.setSearchText(text)}
                    onSubmitEditing={() => this.Get_Stores('reset')}
                    autoFocus={this.state.focusOnSearch}
                    value={this.state.name} />
                  {this.state.name != "" ?
                    <TouchableOpacity onPress={() => this.Get_Stores('reset')}>
                      <Icon style={[{ color: "#111", alignSelf: 'center' }]} type="MaterialIcons" name="search" />
                    </TouchableOpacity>
                    : null}
                </View>

              </View>
             
          </View>
          <View style={{ flex: 1 }} >

            <View style={{ flex: 1, backgroundColor: "#fff",  }}>
              {this.state.data.length > 0 ?
                <List
                  scrollEnabled={this.state.enablescroll}
                  dataArray={this.state.data}
                  ListFooterComponent={<View />}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                  onEndReachedThreshold={this.state.endThreshold}
                  onEndReached={() => {
                    console.log("loadmore")
                    {
                      !this.state.fetching_from_server ?
                      this.Get_Stores('scroll')
                      : null
                    }
                  }}
                  keyExtractor={(item, index) => index.toString()}
                  renderRow={(rowData, sectionID, index) => {
                    let value = rowData;
                    console.log(value)
                    return (
                      <ListItem key={index} onPress={() => this.props.navigation.navigate("Store", { data: value })}>
                        <Left style={{ flex: 0.3 }}>
                          <Thumbnail large resizeMethod='auto' resizeMode="contain"
                            source={{ uri: value.image }} >
                          </Thumbnail>
                        </Left >
                        <Body style={{ flex: 0.8 }}>
                          <View >
                            <Text numberOfLines={1} style={styles.TextList}>{value.name}</Text>
                            <Text numberOfLines={2} note>{value.description}</Text>
                            <Text numberOfLines={1} note><Icon name="clock-outline" type="MaterialCommunityIcons" style={{ fontSize: 16, color: "#808080" }} /> Opened : {value.open_at}</Text>
                            <Text numberOfLines={1} note><Icon name="clock-outline" type="MaterialCommunityIcons" style={{ fontSize: 16, color: "#808080" }} /> Closed : {value.close_at}</Text>
                          </View>
                        </Body>
                      </ListItem>
                      
                    )
                  }}
                />
                :
                <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                  <Text style={styles.TextBrand}> There is no data available ! 😴</Text>
                </View>}

            </View>
            {this.state.fetching_from_server ? (
              <SmallLoader/>
            ) : null}
          </View>


        </View>
        : <Loader />
    )
  }
}
