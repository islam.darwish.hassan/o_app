import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, ListItem, Input, Label, Icon, Text, Button, View, Right, Footer } from 'native-base';
import { API, APIV, Home,Locate,Nearest_Stores,FAKE_TOKEN } from "../../functions/config";
import axios from "axios";
import Slider from "../../components/Slider/Slider";
import AdSwiper from "../AdSwiper";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { Dimensions } from 'react-native';
import Loader from "../Loader";
import StoreMan from "../../functions/storage";
import MapView from 'react-native-maps';
// import * as Permissions from 'expo-permissions'
 import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

export default class componentName extends Component {
  //construction
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };

  state = {
    token: null,
    showMap:true,
    nearest_stores:[],
    ads: [],
    top_stores: [],
    top_discounts: [],
    top_products: [],
    region:{
      // latitude: location.coords.latitude, //Users current position
       // longitude: location.coords.longitude,
       latitude:0.001,
       longitude:0.001,
        latitudeDelta: 0.045, //Deltas set the zoom of the map on screen
        longitudeDelta: 0.045,
      }
  }
  componentDidMount = async () => {
    await this.setState({ isReady: false })
    const stored_token = await this.Storager.get_item("USER_TOKEN");
   
    await this.setState({ token: stored_token })
    await this.Get_Home();
    await this._getLocationAsync();
    await this.Get_Nearest_Stores();
     
    await this.setState({ isReady: true })

  }

  
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      
       // alert('Permission to access location was denied ,please allow ')
        // Linking.openURL('app-settings:');
        this.setState({isReady:true,showMap:false})
      
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
   await this.sentlocation(location.coords.latitude,location.coords.longitude) 
  };

  getRegionFromLocation(location) {
    if (location)
      return ({
      // latitude: location.coords.latitude, //Users current position
       // longitude: location.coords.longitude,
       latitude:0.001,
       longitude:0.001,
        latitudeDelta: 0.045, //Deltas set the zoom of the map on screen
        longitudeDelta: 0.045,
      })
    else
      return ({
        latitude:0.001,
        longitude:0.001,
         latitudeDelta: 0.045, //Deltas set the zoom of the map on screen
         longitudeDelta: 0.045,
 
      })
  }
  sentlocation = async () => {
    var self = this;
    const token = this.state.token;
    var bodyParameters = {
      lat: this.state.region.latitude,
      long: this.state.region.longitude
    }
    var config = {
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        'Authorization': " Bearer " + token
      }
    }
    await axios
      .post(API + APIV + Locate, bodyParameters,
        config
      )
      .then(result => {
        console.log("Location sent");
      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
  }
 

  //requests
  Get_Home = async () => {
    await axios
      .get(API + APIV + Home, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
        this.setState({
          ads: result.data.data.ads,
          top_stores: result.data.data.top_stores,
          top_discounts: result.data.data.top_discounts,
          top_products: result.data.data.top_products
        })
        console.log("Home have been set");

      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });

  }
  Get_Nearest_Stores = async () => {
    await axios
      .get(API + APIV + Nearest_Stores, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
        this.setState({
          nearest_stores: result.data.data.nearest_stores,
        })
        console.log("GPS Stores have been set");

      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          //alert(error.response.data.error);
        }
      });

  }

  render() {
 
    return (
      this.state.isReady ?
        <Content>
          <AdSwiper navigation={this.props.navigation} slides={this.state.ads} link={""} mode="stretch" height={deviceHeight / 3} />

          <ListItem noBorder>
            <Text note>Nearst Stores</Text>
          </ListItem>
          {this.state.showMap ? <MapView style={{ alignSelf: 'stretch', height: 200 }}
            showsUserLocation={true}
            followsUserLocation={false}

            initialRegion={{
              latitude: this.state.location.coords.latitude,
              longitude: this.state.location.coords.longitude,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            }}
          >
            {/* <MapView.Marker
              coordinate={{
                latitude: this.state.location.coords.latitude,
                longitude: this.state.location.coords.longitude,
                }}
              title={"You"}
              description={"Your location"}
            /> */}
            {this.state.nearest_stores.map((value,index)=>{
              return(
                <MapView.Marker
                key={index}
                coordinate={{
                  latitude: value.latitude,
                  longitude: value.longitude,
                }}
                title={value.name}
                description={value.address}
              />
              )
            })}
          </MapView> : null}

          <ListItem noBorder  >
            <Text note >Top Stores</Text>
          </ListItem>
           <Slider
           navigation={this.props.navigation} type={"stores"} data={this.state.top_stores} imagelink={""} />
           <ListItem noBorder  >
            <Text note >Top Products</Text>
          </ListItem>
          <Slider navigation={this.props.navigation} type={"products"} data={this.state.top_products} 
          imagelink={"https://oappweb.com/files/products/images/"} />
           <ListItem noBorder  >
            <Text note >Top Discounts</Text>
          </ListItem>
          <Slider navigation={this.props.navigation} type={"discounts"} data={this.state.top_discounts} 
          imagelink={"https://oappweb.com/files/products/images/"} />

        </Content>
        : <Loader />
    )
  }
}
