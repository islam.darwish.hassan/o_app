import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,FooterTab } from 'native-base';
import { TouchableOpacity } from 'react-native';
import styles from "./style.js";
import Home from "./Tabs/home";
import Discounts from "./Tabs/discounts";
import Search from "./Tabs/search";
import Cart from "./Tabs/cart";
import Dev from"./underdeveloper";
//redux 
import { connect } from "react-redux";
import { addItem,  deleteItem,setItems } from "../store/actions/index";
import search from './Tabs/search';

 class HomeTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
        };
    }
    toggleTab1() {
        this.setState({
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
        });
    }
    toggleTab2() {
        this.setState({
            tab1: false,
            tab2: true,
            tab3: false,
            tab4: false,
        });
    }
    toggleTab3() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
        });

    }
    toggleTab4() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: false,
            tab4: true,

        });
    }

  render() {
    return (

        <Container>
        <Header style={{backgroundColor:"#FF952B"}}>
      <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between',alignItems:"center" }}>
      <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}><Icon  style={{color:"#fff",}} type="MaterialCommunityIcons" name="reorder-horizontal"  size={20}/></TouchableOpacity>
      <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle,{paddingHorizontal:0.5,flex:1,fontSize:18,color:"white"}]}> O-Applciation</Text>
    

        </View>
      </Header>

              <Content >
                  {this.state.tab1 ? (
                      <Home  navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab2 ? (
                      <Discounts navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab3 ? (
                      <Search navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab4 ? (
                      <Cart navigation={this.props.navigation} />
                      ) : null}
              </Content>
              <Footer>
                      <FooterTab>
                          <Button vertical active={this.state.tab1} onPress={() => this.toggleTab1()}>
                          <Icon active={this.state.tab1} type="MaterialIcons" name="home" />
                              <Text>Home</Text>
                          </Button>
                          <Button  vertical active={this.state.tab2} onPress={() => this.toggleTab2()}>
                          <Icon active={this.state.tab2} type="MaterialIcons" name="local-offer" size={18} />
                              <Text>Discounts</Text>
                          </Button>
                              <Button vertical active={this.state.tab3} onPress={() => this.toggleTab3()} >
                              <Icon active={this.state.tab3} type="MaterialIcons" name="search" />
                                  <Text >Search</Text>
                              </Button>
                          <Button vertical active={this.state.tab4} onPress={() => this.toggleTab4()}>
                          <Icon active={this.state.tab4} type="MaterialIcons" name="shopping-cart"  />
                          {this.props.RX_items.length>0?<View style={[{ position:'absolute', top:-20,  paddingHorizontal :10
                            , paddingVertical:5, borderRadius :50 , } , !this.state.tab4 ?{ backgroundColor:'#FF952B'} : {backgroundColor:'#111'}]}>
                            <Text style={[{fontSize:12}, !this.state.tab4 ?{ color:'#111'} : {color:'#FF952B'}]}>{this.props.RX_items.length}</Text>
                            </View>
                        :null}
                          <Text  >Cart </Text>
                          </Button>
                      </FooterTab>
              </Footer>
      </Container>

        )
  }
}
const mapStateToProps = state => {
    return {
      RX_items: state.cart.items,
      RX_selectedItem: state.cart.selectedItem,
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
      OnAdd: item => { dispatch(addItem(item)) },
      OnRemove: item => { dispatch(deleteItem(item)) },
      OnSetAll: items => { dispatch(setItems(items)) }
    };
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(HomeTabs);
  