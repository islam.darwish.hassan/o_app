import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Radio, Text, Button, View, Right, Icon,ListItem } from 'native-base';
import styles from "./style.js";
import { ImageBackground, Image, TouchableOpacity, Dimensions } from "react-native";
import axios from "axios";
import { API, APIV, Sign } from "../functions/config";
import StoreMan from "../functions/storage";
import StepIndicator from 'react-native-step-indicator';
import DatePicker from "../components/DatePickerCustom";
import Govrn from "../components/govrn_picker";
import Gender from "../components/RadioBtn";
import Modal from "react-native-modal";
import {Keyboard} from 'react-native'

import Imgpicker from "../components/imagepickerbase64";
import {
  SkypeIndicator,
} from 'react-native-indicators';
const labels = ["", "", "", ""];
const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 40,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#FF952B',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#FF952B',
  stepStrokeUnFinishedColor: '#aaa',
  separatorFinishedColor: '#FF952B',
  separatorUnFinishedColor: '#fff',
  stepIndicatorFinishedColor: '#FF952B',
  stepIndicatorUnFinishedColor: '#fff',
  stepIndicatorCurrentColor: '#fff',
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: '#FF952B',
  stepIndicatorLabelFinishedColor: '#fff',
  stepIndicatorLabelUnFinishedColor: '#aaa',
  labelColor: '#999999',
  labelSize: 15,
  currentStepLabelColor: '#fff'
}
import { StackActions, NavigationActions } from 'react-navigation';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class SignUp extends Component {

  //states
  state = {
    isReady: true,
    email: "",
    password: "",
    first_name: "",
    last_name: "",
    token: "",
    name: "",
    address: "",
    country_en: "",
    country_id: "",
    currentPosition: 0,
    current: 0,
    city_id: 1,
    governrate_id: 1,
    birth_date: "",
    phone: '',
    gender: "",
    avatar: "",
    isModalVisible:false,
  }
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };



  componentDidMount = async () => {
    const { navigation } = this.props;
    const Data = navigation.getParam('data', '');
    let phoneNumberedited = (Data.phone).replace('+2','');
    await this.setState({ phone: phoneNumberedited });
    console.log('your phone number is', this.state.phone)
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible});
  };


  changeGender=(value)=>{
    this.toggleModal()
    if(value=='m')
    this.setState({gender:'m'})
    else if(value=="f")  
    this.setState({gender:'f'})
    else 
    this.setState({gender:''})
  }

   
  //requests
  Sign = async () => {
    this.setState({ isReady: false });
    //here you are login request 
    await axios
      .post(API + APIV + Sign, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation,
        phone: this.state.phone,
        address: this.state.address,
        "country_id": 66,
        governrate_id: this.state.governrate_id,
        city_id: this.state.city_id,
        birth_date: this.state.birth_date,
        gender: this.state.gender,
        avatar: this.state.avatar,



      })
      .then(result => {
        console.log("User has been registered");
        this.Storager.set_item("USER_TOKEN", result.data.data.user.token);
        this.Storager.set_item("USER_EMAIL", result.data.data.user.email);
        this.Storager.set_item("USER_CITY", result.data.data.user.city.en_name);
        this.Storager.set_item("USER_GOVRN", result.data.data.user.governrate.en_name);
        //this.Storager.set_item("USER_AVATAR",result.data.data.avatar);
        this.Storager.set_item("USER_IDKEY", result.data.data.user.id.toString());
        this.navigateToLogin();
        alert("Hello!" + result.data.data.user.email);
        this.setState({
          email: "",
          password: "",
          password_confirmation: '',
          first_name: "",
          last_name: "",
          phone: "",
          address: "",
          country_en: "",
          country_id: "",
          city_id: 0,
          governrate_id: 0,
          birth_date: "",
          gender: "",
          avatar: ""
        })
      })
      .catch(function (error) {
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({ isReady: true });

  }



  navigateToLogin() {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
      })
    )
  }


  //render
  render() {
    return (
      this.state.isReady ?
        <Container style={{ backgroundColor: "white", height: deviceHeight }} >
          <Header style={{ backgroundColor: "#FF952B", alignItems: "center", justifyContent: "center" }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: "white" }}>Sign Up</Text>
          </Header>
          <Content>
            <View style={{ height: deviceHeight * 0.5 }} padder >
              <Form style={{ flex: 1, alignItems: "space-around", alignContent: "space-around", justifyContent: "center" }}>
                <View>
                  {this.state.current == 0 ?
                    <View style={{ flex: 1, justifyContent: 'space-evenly', alignSelf: "center", alignItems: "space-around" }}>
                      <View style={{ alignSelf: "flex-start" }}><Text style={{ fontSize: 22, color: "#FF952B", fontWeight: "bold" }}>Welcome to Our family !</Text></View>
                      <Item style={{ width: deviceWidth - 20 }} stackedLabel>
                        <Label style={[styles.TitleBigInput, { marginBottom: 5 }]}>Your First Name *
                </Label>
                        <Input
                          style={{ marginTop: 0, fontSize: 20 }}
                          onChangeText={text => this.validate_UNa(text, "firstname")}
                          value={this.state.first_name}
                        />
                      </Item>
                      <Item style={{ width: deviceWidth - 20 }} stackedLabel>
                        <Label style={[styles.TitleBigInput, { marginBottom: 5 }]}>Your Last Name *
                </Label>
                        <Input
                          style={{ marginTop: 0, fontSize: 20 }}
                          onChangeText={text => this.validate_UNa(text, "lastname")}
                          value={this.state.last_name}
                        />
                      </Item>
                      <Item  style={{ width: deviceWidth - 20 }}  stackedLabel>
                        <Label style={[styles.TitleBigInput, { marginBottom: 5 }]}>Gender *
                </Label>
                        <TouchableOpacity onPress={() => this.toggleModal()}>
                          {this.state.gender ?
                            <Text style={{ fontSize: 16, color: "#000",textAlign:'left' }}>
                              {this.state.gender === "m" ? "Male " : 'Female '
                              }</Text>
                            :
                            <Text style={{ fontSize: 16, color:"#FF952B" }}>Select gender </Text>
                          }
                        </TouchableOpacity>
                      </Item>

                      {/* <Item style={{ width: deviceWidth - 20 }}>
                        <DatePicker dateChange={this.handleDate} date={this.state.birth_date} />
                      </Item> */}

                      
          <Modal isVisible={this.state.isModalVisible} onBackdropPress={Keyboard.dismiss}>
                  <View style={{height:deviceHeight/6,backgroundColor:'black',width:deviceWidth/1.2,alignSelf:"center"
                  ,borderRadius:deviceWidth/10}} >
                    <View style={{
                      backgroundColor: "#fff",borderRadius:deviceWidth/20,flex:1,alignContent:'space-between'
                    }}>
                      <ListItem noIndent	 style={{ justifyContent: "space-between" }}>
                        <Text style={{fontSize:15,color:'#000'}}>Choose your gender ! </Text>
                        <Icon name="close" type="MaterialIcons" style={{ color: "#707070" }} onPress={() => this.toggleModal()} />
                      </ListItem>
                      <View style={{flexDirection:"row" ,alignItems:'center',flex:1 ,marginTop:deviceHeight/60,justifyContent:'center'}}>
                      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 5 }}>
                        <Button 
                          title="Hide modal" style={[{
                            justifyContent: "center", marginHorizontal: 2,
                            width: 100, height: 40, borderRadius: 5,backgroundColor: "#FF952B"
                          } ]} onPress={() => this.changeGender('m')} >
                          <Text style={{ color: "#fff" }}>Male</Text></Button>
                      </View>
                      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 5 }}>
                        <Button 
                          title="Hide modal" style={[{
                            justifyContent: "center", marginHorizontal: 2,
                            width: 100, height: 40, borderRadius: 5,backgroundColor: "#FF952B"
                          } ]} onPress={() => this.changeGender('f')} >
                          <Text style={{ color: "#fff" }}>Female</Text></Button>
                      </View>
                      </View>

                    </View>

                  </View>
                </Modal> 
                    </View>
                    : this.state.current == 1 ?
                      <View style={{ flex: 1, justifyContent: 'space-evenly', alignSelf: "center", alignItems: "space-around" }}>
                        <Item stackedLabel>
                          <Label style={styles.TitleBigInput} >Write your E-mail * </Label>
                          <Input
                            style={{ fontSize: 20 }}
                            onChangeText={text => this.validate_EM(text, "email")}
                            value={this.state.email}
                          />
                        </Item>
                        <Item stackedLabel >
                          <Label style={styles.TitleBigInput}>Write your password *</Label>
                          <Input
                            style={{ fontSize: 20 }}
                            secureTextEntry
                            onChangeText={text => this.validate_PS(text, "password")}
                            value={this.state.password}
                          />
                        </Item>
                        <Item stackedLabel >
                          <Label style={styles.TitleBigInput}>Confirm your password *</Label>
                          <Input
                            style={{ fontSize: 20 }}
                            secureTextEntry
                            onChangeText={text => this.validate_PSC(text, "password_confirm")}
                            value={this.state.password_confirmation}
                          />
                        </Item>

                      </View>
                      : this.state.current == 2 ?
                        <View style={{ flex: 1, justifyContent: 'space-evenly', alignSelf: "center", alignItems: "space-around" }}>
                          <View style={{ width: deviceWidth - 20 }}>
                            <Govrn cityChange={this.handleCity} governChange={this.handleGovrn} />
                          </View>
                          <View style={{
                            flex: 0.3, width: deviceWidth - 20, alignContent: 'center'
                            , justifyContent: 'center'
                          }}>
                            <Item stackedLabel >
                              <Label  >Write your Address </Label>
                              <Input
                                style={{ fontSize: 20 }}
                                onChangeText={text => this.validate_UNa(text, "address")}
                                value={this.state.address}
                              />
                            </Item>
                          </View>
                        </View>
                        : this.state.current == 3 ?
                          <View style={{ flex: 1, justifyContent: 'space-evenly', alignSelf: "center", alignItems: "space-around" }}>
                            <Item stackedLabel
                            >
                              <Label style={styles.TitleBigInput}>What's your phone Number ? </Label>
                              <Input
                                disabled={true}
                                value={this.state.phone}
                              />
                            </Item>
                            <Item style={{ alignSelf: "center", alignItems: "center", borderBottomWidth: 0 }} >
                              <View style={{ alignSelf: "center" }}>
                                <Imgpicker
                                  imgChange={this.onImgchange.bind(this)}
                                />
                              </View>
                            </Item>
                            {/* <Item style={{ borderBottomWidth: 0 }}>
                              <Gender genderChange={this.handleGender} gender={this.state.gender} />
                            </Item> */}

                          </View>
                          : null}
                </View>
              </Form>
            </View>
            <View style={{ height: deviceHeight * 0.1 }} padder>
              <View style={{ width: deviceWidth / 3, flexDirection: "row", alignContent: "center", alignSelf: "center", alignItems: "space-around", justifyContent: "space-between" }}>
                {this.state.current > 0 ?
                  <TouchableOpacity onPress={() => { this.setState({ current: this.state.current - 1 }) }}  >
                    <Icon name='arrow-left-thick' type="MaterialCommunityIcons" style={styles.HeaderIcon} />
                  </TouchableOpacity >
                  : <TouchableOpacity onPress={() => { this.props.navigation.navigate("Login") }}   >
                    <Icon name='arrow-left-thick' type="MaterialCommunityIcons" style={styles.HeaderIcon} />
                  </TouchableOpacity >
                }
                <Text style={styles.TitleBigInputBold}>{this.state.current + 1}</Text>

                {this.state.current < 3 ?
                  <TouchableOpacity onPress={() => { this.setState({ current: this.state.current + 1 }) }}  >
                    <Icon name='arrow-right-thick' type="MaterialCommunityIcons" style={[styles.HeaderIcon]} />
                  </TouchableOpacity >
                  :
                  <TouchableOpacity onPress={() => { this.Sign() }}  >
                    <Icon name='check-all' type="MaterialCommunityIcons" style={[styles.HeaderIcon, { color: "#FF952B" }]} />
                  </TouchableOpacity >
                }
              </View>
            </View>
            <View style={{ height: deviceHeight * 0.2 }} padder>
              <StepIndicator
                customStyles={customStyles}
                currentPosition={this.state.current}
                labels={labels}
                stepCount={4}
              />
              <View style={{ alignSelf: "center", alignItems: "center", paddingBottom: 10 }} ><Text note>Are you already one of family ? </Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate("Login") }}  ><Text style={styles.TextLink}> Login </Text></TouchableOpacity >
              </View>
            </View>

          </Content>
        </Container>
        : <Container style={styles.LoaderBackground}>
          <SkypeIndicator color='#FF952B' />
        </Container>
    );
  }
  //handlers
  handleCity = async (data) => {
    await this.setState({ city_id: data });
    //console.log(this.state.city_id);
  }
  handleGovrn = async (data) => {
    await this.setState({ governrate_id: data });

  }
  onImgchange(image) {
    var self = this;

    self.setState({ avatar: image });
    //console.log(this.state.avatar);
  }
  handleDate = async (data) => {
    await this.setState({ birth_date: data });

  }
  handleGender = async (data) => {
    await this.setState({ gender: data });

  }
  //validation
  validate_UNa = (text, type) => {
    if (type == 'firstname') {
      var self = this;
      self.setState({
        first_name: text
      });
      if (text.length < 2) {
        self.setState({ FirstNameError: true });
      }
      else {
        self.setState({ FirstNameError: false });

      }
    }
    if (type == 'lastname') {
      var self = this;
      self.setState({
        last_name: text
      });
      if (text.length < 2) {
        self.setState({ LastNameError: true });
      }
      else {
        self.setState({ LastNameError: false });

      }
    }
    if (type == 'address') {
      var self = this;
      self.setState({
        address: text
      });
      if (text.length < 3) {
        self.setState({ AddressError: true });
      }
      else {
        self.setState({ AddressError: false });

      }
    }
  }
  validate_PS = (text, type) => {

    var self = this;

    if ((type = "password")) {
      self.setState({
        password: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }
  validate_EM = (text, type) => {
    var self = this;

    let alph = /^[a-zA-Z]+$/
    if ((type = "email")) {
      self.setState({
        email: text
      });
      if (text.length < 5 || text.indexOf("@") === -1) {
        self.setState({ VEmailError: true });
      } else {
        self.setState({ VEmailError: false });

      }
    }
  }
  validate_PSC = (text, type) => {
    var self = this;

    if ((type = "password_confirm")) {
      self.setState({
        password_confirmation: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }
  validate_PH = (text, type) => {
    var self = this;

    let num = /^[0-9]+$ /
    if ((type = "phone")) {
      self.setState({
        phone: text
      });
      if ((text.length < 7) || (text.length > 12)) {
        self.setState({ MobileError: true });
      } else {
        self.setState({ MobileError: false });
      }
    }
  }

}