import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left, Body, Thumbnail
} from 'native-base';
import { API, APIV, Product } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image, Linking, Dimensions, ScrollView, FlatList } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from "../Loader";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import StoreMan from "../../functions/storage";
//redux 
import { connect } from "react-redux";
import { addItem, deleteItem, setItems } from "../../store/actions/index";
import { Rating, AirbnbRating } from 'react-native-ratings';

// Fake Data to be added instead of Back end Data // 
const fakedata = {
  images: ["https://www.laptopmag.com/images/wp/purch-api/incontent/2019/04/img_20190416_164221.jpg",
    "https://i.ytimg.com/vi/oPGxOmP3jOQ/maxresdefault.jpg",
    "https://www.laptopmag.com/images/wp/purch-api/incontent/2019/04/img_20190416_164221.jpg",
    "https://www.lenovo.com/medias/lenovo-laptop-legion-y920-17-hero.png?context=bWFzdGVyfHJvb3R8OTYyNzR8aW1hZ2UvcG5nfGgyMi9oN2UvOTQ3MjYyMzYwNzgzOC5wbmd8YWVmOGU2YzY4ZGQzYTg5ZDNhZDZiNWMxOGIwODRiYmFjYTg0ZTQ1NjVkMzhlNjQ4NzM2MThkYmI2NDlhZDU3Zg"
  ],
}
 class ProductPage extends Component {

  //construction
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };


  state = { data: null, isReady: false }

  componentDidMount = async () => {
    const { navigation } = this.props;
    const Data = navigation.getParam('data', '');
    await this.setState({ data: Data });
    await this.setState({ isReady: true });
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({ token: stored_token })
    await this.Get_Product();

  }

  //request 
  Get_Product = async () => {
    await axios
      .get(API + APIV + Product + this.state.data.id, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
        this.setState({
          data: result.data.data.product,
        })
        console.log("Product has been set");
      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          //alert(error.response.data.error);
        }
      });

  }
  
  AddToCart = async () => {
      let product=this.state.data;
      const newItem = {
          id: product.id,
          name:product.name,
          description: product.description,
          image: product.image,
          qty: 1,
          note: "nothing",
          price:product.price,
          discount:product.discount,
          rate:product.rate,

      }
      await this.props.OnAdd(newItem);
      this.props.navigation.goBack();
      console.log(this.props.RX_items);
  
}
  render() {
    data = this.state.data;
    return (
      this.state.isReady ?
        <Container>
          <Header style={{ backgroundColor: "#FF952B", justifyContent: "space-between"
          , alignItems: "center" }}>
            <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between',
             alignItems: 'flex-start' }}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} />
              </TouchableOpacity>
              <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.HeaderTitle, 
                { paddingHorizontal: 0.9, flex: 1, fontSize: 18, color: "white", textAlign: 'justify' }]}> 
                 {data.name}</Text>
            </View>
          </Header>
          <Content>
            <TouchableOpacity > 
            <Image style={styles.ImageHeader} source={{ uri: data.image }} />
            </TouchableOpacity>
            <List>
              <ListItem>
                <Text style={styles.TextSmalBlackBold}>{data.name}</Text>
              </ListItem>
              <ListItem>
                <Text style={styles.TextSmalBlack}><Text note> Price:</Text> {data.price}</Text>
              </ListItem>
              <ListItem style={{alignItems:'center'}}>
              <Text style={styles.TextSmalBlack} ><Text note> Rate:</Text>    </Text>
              <TouchableOpacity
              onPress={()=>this.props.navigation.navigate("AddReview", { data: this.state.data.id })}
              >

              <Rating
                          
                          type='star'
                          startingValue={data.rate}
                          readonly
                          fractions={2}
                           ratingColor={data.rate < 3 ? '#FEC001' :data.rate <=6 ?'#FEC001':'#FEC001'}
                          
                          ratingBackgroundColor='#c8c7c8'
                          ratingCount={5}
                          imageSize={20}
                          style={{ padding: 2, margin: 0,alignSelf:'flex-start'}}
                          
                        />      
                  </TouchableOpacity>

              </ListItem>

              {data.discount.has_discount == true ? (
              <ListItem><Text><Text note> Discount:</Text> {data.discount.value} </Text></ListItem>
              ) : null}
              <ListItem >
                  <Text note > Description:</Text>
             </ListItem>
             <ListItem>
                    <Text>{data.description}</Text>
             </ListItem>
              <ListItem>
              <Text><Text note> Stock:</Text> {data.stock}</Text>
              </ListItem>
              {data.reviews?
                <View  >
                  <ListItem>
                  <Text note> Reviews:</Text>
                 </ListItem>
                 {data.reviews.length>0 ?
                  <Text>{data.reviews}</Text>
                  :
                  <Text style={[styles.TextSmalGreyBold,{alignSelf:"center",marginVertical:10}]}> There is No Reviews Yet ! </Text>}
              </View>
              :null} 
            </List>
            <View >
              <ScrollView horizontal={true}>
              {data.images?
                data.images.map((value, index) => {
                  /* map function of an array similars to foreach function this fn will return(index , value) 
                  -index is the number of element in array 
                  -value is the value of this element
                  */
                  return (
                    <Image key={index} style={styles.ImageHeader} source={{ uri: value }} />
                  )
                })
               :null }
              </ScrollView>
            </View>
            <TouchableOpacity style={[styles.CartButton,{marginBottom:20}]} onPress={()=>this.AddToCart()} >
            <Text style={styles.CardButtonText}>Add To Cart</Text>
            </TouchableOpacity>
          </Content>
        </Container> : <Loader />
    )
  }
}
const mapStateToProps = state => {
  return {
    RX_items: state.cart.items,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    OnAdd: item => { dispatch(addItem(item)) },
    OnRemove: item => { dispatch(deleteItem(item)) },
    OnSetAll: items => { dispatch(setItems(items)) }
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
