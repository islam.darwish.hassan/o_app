import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem,List,Left,
  Body, 
  Thumbnail} from 'native-base';
import { API, APIV, Store } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from"../Loader";
import StoreMan from "../../functions/storage";
import SmallLoader from './SmallLoader';

export default class StorePage extends Component {
    //construction
    constructor(props) {
      super(props);
      this.Storager = new StoreMan();
  };
  
    state={token:null,data:[] ,
      fetching_from_server: false,
      products:[],
      page:2,
      refreshing: false,
      isReady:false,products:[] }

    componentDidMount=async()=>{
        const { navigation } = this.props;
         const Data = navigation.getParam('data', '');
        await this.setState({data:Data});
        const stored_token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({token:stored_token})
        await this.Get_Products('reset');
        await this.setState({isReady:true});

    }
    Get_Products = async (mode) => {
      this.setState({fetching_from_server: true});
      const tokens = this.state.token;
      if (mode === 'reset') {
      await axios
        .get(API + APIV + Store + this.state.data.id + "/products"  + "?page=1"
        , {
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": " Bearer " + tokens

          },
        })
        .then(result => {
          this.setState({
            data:result.data.data.store,
            products:result.data.data.products,
            page: 2 // next 
          })
          console.log("Products have been set");
  
        })
        .catch(function (error) {
          console.log(error);
          if (error.response) {
            //error handling
            alert(error.response.data.error);
          }
        });
  
    }else { 
      console.log('get data of page -> ' + this.state.page)
      await axios
      .get(API + APIV + Store + this.state.data.id + "/products"  +  "?page=" +this.state.page
      , {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + tokens
        },

      })
      .then(result => {
        console.log(result.data.data.products.length)
  
          if (result.data.data.products.length > 0) {
            console.log("iamhere")
           // this.setState({ data: [...this.state.data, ...result.data.data.store] })
            this.setState({ products: [...this.state.products, ...result.data.data.products] })
            this.setState({ page: this.state.page + 1 })

          } else {
            console.log("endreached")

          }

        

      })
      .catch(error => {
        // console.log(error.response.data.error);
      });

    }
    console.log(this.state.products.length);
    this.setState({ fetching_from_server: false });
    }



    
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.Get_Products('reset').then(() => {
      this.setState({refreshing: false});
    });
  }   



  render() {
    return (
      this.state.isReady ? 
      <View style ={{flex:1}}>
        <Header style={{ backgroundColor: "#FF952B" ,justifyContent:"space-between" ,alignItems:"center"}}>
          <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
            <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> {this.state.data.name}</Text>
          </View>
          <View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("StoreInfo",{data:this.state.data})}>
          <Icon style={{ color: "#fff", }} type="MaterialIcons" name="info" size={20} /></TouchableOpacity>
          </View>
        </Header>
        <View style={{ flex: 1 }} >

<View style={{ flex: 1, backgroundColor: "#fff",  }}>
        {this.state.products.length > 0 ?
                <List
                  scrollEnabled={this.state.enablescroll}
                  dataArray={this.state.products}
                  ListFooterComponent={<View />}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                  onEndReachedThreshold={this.state.endThreshold}
                  onEndReached={() => {
                    console.log("loadmore")
                    {
                      !this.state.fetching_from_server ?
                      this.Get_Products('scroll')
                      : null
                    }
                  }}
                  keyExtractor={(item, index) => index.toString()}
                  renderRow={(rowData, sectionID, index) => {
                    let value = rowData;
                    //console.log(value)
                    return (
                        <ListItem  key={index} onPress={()=>this.props.navigation.navigate("Product",{
                            data:value
                          })}>
                          <Left style={{flex:0.3}}>
                          <Thumbnail large resizeMethod='auto' resizeMode="contain"
                          source={{ uri: value.image }} >
                          </Thumbnail>
                          </Left >
                            <Body style={{flex:0.8, }}>
                              <View >
                                <Text numberOfLines={2} style={[styles.TextList],{width:"95%",fontWeight:'bold' }}>{value.name}</Text>
                                <Text numberOfLines={2} note>{value.description}</Text>
                              </View>
                            </Body>
                            {value.discount.has_discount?
                            <View style={styles.Offerbadge}><Text style={styles.TextSmallWhite}>{value.discount.value}</Text></View>
                            :null}
                          </ListItem>
                      
                    )
                  }}
                />
                :
                <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                  <Text style={styles.TextBrand}> There is no data available ! 😴</Text>
                </View>}

                </View>
            {this.state.fetching_from_server ? (
              <SmallLoader/>
            ) : null}
          </View>
      </View>
      : <Loader/>
  
    )
  }
}
