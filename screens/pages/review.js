import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left, Body, Thumbnail
} from 'native-base';

import styles from "../style";
import { Image, Linking, Dimensions, ScrollView, FlatList, TextComponent } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Loader from "../loader";

import { Rating, AirbnbRating } from 'react-native-ratings';
import ReadMore from 'react-native-read-more-text';

export default class review extends Component {

     //construction
     constructor(props) {
        super(props);
       };
  
      state = { token:'',data: null, isReady: false,product_id:'',reviews:[], quantity: 1,userState:'' ,page: 1,
    fetching_from_server: false,}
  
    componentDidMount = async () => {
        await this.setState({ isReady: false });

      console.log("data from parent is ",this.props.data)
      await this.setState({ data:this.props.data});

      await this.setState({ isReady: true });

      
    }
    render() {
        let value = this.state.data
        return (
            this.state.isReady?
            <ListItem style={{flexDirection:'column',justifyContent:'space-between',flex:1}}>
                <View style={{width:deviceWidth/1.1,flexDirection:'row',justifyContent:'space-between'}}>
                <Text>{value.client.name}</Text>
                <Rating
                          
                          type='star'
                          startingValue={value.rate}
                          readonly
                          fractions={2}
                           ratingColor={value.rate < 3 ? '#FEC001' :value.rate <=6 ?'#FEC001':'#FEC001'}
                          
                          ratingBackgroundColor='#c8c7c8'
                          ratingCount={5}
                          imageSize={20}
                          style={{ padding: 2, margin: 0,alignSelf:'flex-start'}}
                          
                        />      
                </View>
                <View style={{flexDirection:'row',marginVertical:10}}>
                <ReadMore
              numberOfLines={3}
              renderTruncatedFooter={this._renderTruncatedFooter}
              renderRevealedFooter={this._renderRevealedFooter}
              onReady={this._handleTextReady}>
              <Text style={[{textAlign:'left',alignSelf:'flex-start',}]}>
                {value.review}</Text>
              </ReadMore>
                </View>
            </ListItem>
            : <Loader/>
        )
    }

    _renderTruncatedFooter = (handlePress) => {
        return (
            <Text style={{ color: "#8CC43E", marginTop: 5 }} onPress={handlePress}>
    read more
            </Text>
        );
        }
    
        _renderRevealedFooter = (handlePress) => {
        return (
            <Text style={{ color: "#8CC43E", marginTop: 5 }} onPress={handlePress}>
    show less
            </Text>
        );
        }

        
}
