import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left,
  Body,
  Thumbnail
} from 'native-base';
//functions
import StoreMan from "../../functions/storage";
//axios 
import axios from "axios";
import { API, APIV, Checkout } from "../../functions/config";
import { StackActions ,NavigationActions} from 'react-navigation';

import { TouchableOpacity, Image, TouchableWithoutFeedback } from "react-native";
import { Dimensions } from "react-native";
const deviceWidth = Dimensions.get("window").width;
const deviceHeigth = Dimensions.get("window").heigth;
import styles from "../style";
import Loader from"../Loader";
//redux 
import { connect } from "react-redux";
import { addItem,  deleteItem,setItems } from "../../store/actions/index";
import { DrawerActions } from 'react-navigation-drawer';

class CheckoutPage extends Component {
      //construction
      constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    };
    state={token:null,data: null ,isReady:false,products:[] }
    componentDidMount = async () => {
      this.setState({ isReady: false });
      const token = await this.Storager.get_item("USER_TOKEN");
      await this.setState({ products: this.props.RX_items, token });
      const { navigation } = this.props;
      const Data = navigation.getParam('data', '');
     await this.setState({data:Data});

      this.setState({ isReady: true });
    }
      Checkout = async () => {
        this.setState({ isReady: false });
        const token = this.state.token;
        var bodyParameters = {
          "note": "hat el order bsr3a",
          products: this.props.RX_items,
    
        }
        var config = {
    
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            'Authorization': " Bearer " + token
          }
        }
        //here you are login request 
        await axios
          .post(API + APIV + Checkout, bodyParameters,
            config)
          .then(result => {
            console.log("Order has been Procced");
            this.props.OnSetAll([]);
            this.navigateToHome()
          })
          .catch(function (error) {
            if (error.response) {
              //error handling
              alert(error.response.data.error);
            }
          });
        this.setState({ isReady: true });
    
      }          
      
      
      
  navigateToHome() {
    this.props.navigation.dispatch(
      StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
      })
      )
    this.props.navigation.dispatch(resetAction);
  }


  render() {
    data=this.state.data;
    return (
      this.state.isReady ?
        <Container>
        <Header style={{ backgroundColor: "#FF952B" }}>
        <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
        <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
        <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> Order Checkout</Text>
        </View>
      </Header>
        <Content padder >
        <ListItem >
        <Body style={{ flex: 1, }}>
          <View >
            {console.log(data)}
    <Text numberOfLines={1} ><Icon name="phone" type="MaterialCommunityIcons" style={{ fontSize: 20, color: "#FF952B" }} /> Phone : {data.phone}</Text>

          <Text numberOfLines={2} style={[styles.TextSmalBlack],{}}><Icon name="map-marker-radius" type="MaterialCommunityIcons" style={{ fontSize: 20, color: "#FF952B" }} /> Address : {data.address}</Text>
          <Text numberOfLines={1} ><Icon name="palette-swatch" type="MaterialCommunityIcons" style={{ fontSize: 20, color: "#FF952B" }} /> Delivery Fees : {data.delivery_fees}</Text>
          <Text numberOfLines={1} style={styles.TextSmalBlack}></Text>
          <Text numberOfLines={1} style={[styles.TextSmalBlack],{fontWeight:'bold'}}>Total Price : {data.total_price}</Text>

          </View> 
          </Body>    
        </ListItem>

          {data.products.length > 0 ?
            data.products.map((value, index) => {
              let product =value
              return (
                <View key={index}>
                <ListItem  onPress={() => this.props.navigation.navigate("Product", {
                  data: product
                })}>
                  <Left style={{ flex: 0.3 }}>
                    <Thumbnail  resizeMethod='auto' square  resizeMode="contain"
                      source={{ uri: product.product_image }} >
                    </Thumbnail>
                  </Left >
                  <Body style={{ flex: 0.8, }}>
                    <View >
                      <Text numberOfLines={1} style={styles.TextList}>{product.product_name}</Text>
                      <Text numberOfLines={2} note>store: {product.store_name}</Text>
                    </View>
                  </Body>
                  <Right style={{ }}>
                  <Thumbnail normal resizeMethod='auto' resizeMode="contain"
                      source={{ uri: product.store_image }} >
                    </Thumbnail>
                  </Right>
                  {product.discount.has_discount?
                    <View style={styles.Offerbadge}><Text style={styles.TextSmallWhite}>{product.discount.value}</Text></View>
                    :null}
                </ListItem>
                <ListItem style={{paddingHorizontal:10,flexDirection:'column',}}>
                <Text style={{alignSelf:'flex-start',justifyContent:'space-between'}}>Discount : {value.discount} </Text>
                <Text style={{alignSelf:'flex-start'}}>Quantity : {value.qty}</Text>
                <Text style={{alignSelf:'flex-start'}}>Final Price : {value.final_price}</Text>
                </ListItem>
                </View>
            );
            }
            ) :
            <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
              <Text style={styles.TitleSmallWhite}> There is an error occured ! 😴</Text>
            </View>
          }
          <ListItem last>
          <Text note >Total Price:  {data.total_price}</Text>
          <Text note> COD (Cash On Delivery)</Text>
          </ListItem>
          {this.props.RX_items.length > 0 ?
          <TouchableOpacity style={[styles.CartButton,{marginVertical:20}]} onPress={()=>this.Checkout()} >
          <Text style={styles.CardButtonText}>Checkout</Text>
          </TouchableOpacity>
          :null}
      </Content>
        </Container>
        : <Loader/>
  
    )
  }
}
const mapStateToProps = state => {
  return {
    RX_items: state.cart.items,
    RX_selectedItem: state.cart.selectedItem,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    OnAdd: item => { dispatch(addItem(item)) },
    OnRemove: item => { dispatch(deleteItem(item)) },
    OnSetAll: items => { dispatch(setItems(items)) }
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);
