import React, { Component } from 'react'
import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left, Body, Thumbnail
} from 'native-base';
import { API, APIV, Profile } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import StoreMan from "../../functions/storage";
import Modal from "react-native-modal";
import * as ImagePicker from 'expo-image-picker';
 
import {Keyboard} from 'react-native'
import { Image, Linking, Dimensions, ScrollView, FlatList, Clipboard ,TextInput,
   BackHandler,Alert ,KeyboardAvoidingView,
   ImageStore,
   ImageEditor,} from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from "../Loader";
import { itemWidth } from '../../components/Slider/styles/SliderEntry.style';
import { stringLiteral } from '@babel/types';
import DatePicker from"../../components/DatePickerCustom";
 import Govrn from"../../components/govrnPickerModified";
 import RawDateTimePicker from "../../components/profileDateTimePicker"

//import { Script } from 'vm';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
 
  

export default class ProfilePage extends Component {

    //construction
    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    };
    
    state = {token: null, data: [], isReady: false, editMode: false,
        fetching_from_server: false, search_text: "",
        endThreshold: 2, text: '',isModalVisible:false,
        first_name:"",
        last_name:"",
        email:"",
        password:"",
        password_confirmation:'',
        birth_date:"",
        gender:"",
        country:{},
        governrate_id:'',
        city_id:'',
        address:"",
        phone:"",
        
          }


    componentDidMount = async () => {
        await this.setState({ isReady: false });
        const stored_token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({token:stored_token})
        
        await this.Get_Profile();
       
        await this.setState({ isReady: true });
        console.log('image is ',this.state.data.avatar)
    
      }

 

    onImgchange(image) {
        var self = this;
    
        self.setState({ avatar: image }); 
        //console.log(this.state.avatar);
      }

   



      Get_Profile = async () => {
        await axios
          .get(API + APIV + "profile", {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": " Bearer " + this.state.token
            },
          })
          .then(result => {
            this.setState({
              data:result.data.data.user,
              first_name: result.data.data.user.first_name,
              last_name: result.data.data.user.last_name,
              gender:result.data.data.user.gender,
              birth_date: result.data.data.user.birth_date,
              address:result.data.data.user.address,
              country:result.data.data.user.country,
              phone:result.data.data.user.phone,
              
            })
            // console.log(data);
    
          })
          .catch(function (error) {
            console.log(error);
            if (error.response) {
              //error handling
              alert(error.response.data.error);
            }
          });
      }
  


      getAge =(dateString) =>
{
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    return age;
}
      toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible});
      };


      changeGender=(value)=>{
        this.toggleModal()
        if(value=='m')
        this.setState({gender:'m'})
        else if(value=="f")  
        this.setState({gender:'f'})
        else 
        this.setState({gender:''})
      }


       

      _pickImg = async () => {
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
          base64: true,
          allowsEditing: false,
          mediaTypes : 'Images',
          aspect: [4, 3],
          quality:0.1
        });
    
    
        if (!pickerResult.cancelled) {
          this.setState({
            pickerResult,
          });
        let imageUri = pickerResult    
        if(imageUri!=null && imageUri!=null && imageUri!="")
         {imageUri= `data:image/jpg;base64,${pickerResult.base64}`   
        }else{
          imageUri=""
        }
        console.log({ uri: imageUri.slice(0, 100) });
        this.setState({ avatar: imageUri });
    
      }else{
    
      }
      }; 


      Update = async () => {
        var self = this;
        self.setState({ isReady: false });
        let token = this.state.token;
        await axios
        console.log(this.state.first_name)
        console.log(this.state.last_name)
        console.log(this.state.birth_date)
         console.log(this.state.address)
        console.log(this.state.phone)
        console.log(this.state.gender)
       // console.log(this.state.avatar)
    
    
        var config = {
            headers: { 'Authorization': " Bearer " + token }
        };
        if(!this.state.pickerResult){
          var bodyParameters =
          {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
           // email: this.state.email,
             //password: this.state.password,
           // password_confirmation: this.state.password_confirmation,
            phone: this.state.phone,
            address:this.state.address,
            "country_id":66,
            governrate_id:this.state.governrate_id ? this.state.governrate_id : this.state.data.governrate.id,
            city_id:this.state.city_id ? this.state.city_id : this.state.data.city.id,
            birth_date:this.state.birth_date,
            gender:this.state.gender,
             avatar:this.state.avatar,
      
      
          }
      
    
        }else{
          var bodyParameters =
          {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
           // email: this.state.email,
            //password: this.state.password,
           // password_confirmation: this.state.password_confirmation,
            phone: this.state.phone,
            address:this.state.address,
            "country_id":66,
            governrate_id:this.state.governrate_id ? this.state.governrate_id : this.state.data.governrate.id,
            city_id:this.state.city_id ? this.state.city_id : this.state.data.city.id,
            birth_date:this.state.birth_date,
            gender:this.state.gender,
            avatar:this.state.avatar,
          }
      
        }
    
        await axios
            .put(API + APIV + 'profile', bodyParameters,
                config)
            .then(res => {
              alert('Profile Updated Sucessfully');
                    console.log("update -> success ");
                    this.saveData(res)
            })
            .catch(function (error) {
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
    
            });
            this.Get_Profile();
            this.setState({ editMode: false,isReady: true })
    }; 




    render() {
        let value = this.state.data;
        return (
            this.state.isReady ?
                <Container>
                <Header style={{ backgroundColor: "#FF952B", justifyContent: "space-between", alignItems: "center" }}>
                <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} />
                  </TouchableOpacity>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.HeaderTitle, { paddingHorizontal: 0.9, flex: 1, fontSize: 18, color: "white", textAlign: 'justify' }]}> My Profile</Text>
                </View>
              </Header>

                    <Content>
                         

                    <View style={{width:deviceWidth/3.5,height:deviceWidth/3.5,marginTop:deviceHeight/15,    // image fixed // 
                borderRadius:deviceWidth/4,alignSelf:'center',borderColor:'#FF952B',borderWidth:3,
                alignContent:'center',justifyContent:'center',alignItems:'center'
                ,backgroundColor:'white'}}>
                 <Image   resizeMode={'cover'} 
                 source={this.state.editMode == true ? {uri:  this.state.avatar ? this.state.avatar : this.state.data.avatar} 
                 :this.state.editMode == false ? {uri: this.state.data.avatar} : null} 
                 style={{width:deviceWidth/3.8,height:deviceWidth/3.8,    // image fixed // 
                  borderRadius:deviceWidth}}/>   
                       {this.state.editMode == true ? 
                       <View style={{position:'absolute',top:deviceHeight/10,left:deviceWidth/5.5,backgroundColor:'#F0EEEE',height:45,width:45,borderRadius:25,alignItems:"center",justifyContent:'center'}}>  
                       <View style={{ backgroundColor:'white',height:37,width:37,borderRadius:25,alignItems:'center' ,justifyContent:'center'}}>
                          
                           <Icon onPress={()=>this._pickImg()}style={{  color: "#535353",fontSize:25 }} type="Entypo" name="camera" size={20} />  
                       </View>  
                     </View> 
                  
                       : null} 
              </View>
      


              {this.state.editMode === false ? 

      <View style={[styles.fullProfileCard,{marginVertical:deviceHeight/20 
            ,paddingHorizontal:deviceWidth/10}]}>
          
          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >First name : </Text>
              <Text style={{fontSize:15}} >{this.state.data.first_name}</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Last name : </Text>
              <Text style={{fontSize:15}} >{this.state.data.last_name}</Text>
          </View>


          {/* <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Email : </Text>
              <Text style={{fontSize:15}} >{this.state.data.email}</Text>
          </View> */}
 
          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Age : </Text>
              <Text style={{fontSize:15}} >{ this.getAge(this.state.data.birth_date) ? this.getAge(this.state.birth_date) : " " }</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Gender : </Text>
              <Text style={{fontSize:15}} >{this.state.data.gender ==="m" ?("Male"):("Female") }</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Country : </Text>
              <Text style={{fontSize:15}} >{this.state.data.country.en_name}</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Governrate : </Text>
              <Text style={{fontSize:15}} >{this.state.data.governrate.en_name}</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >City : </Text>
              <Text style={{fontSize:15}} >{this.state.data.city.en_name}</Text>
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Address : </Text>
              <Text style={{fontSize:15}} >{this.state.data.address}</Text>
          </View>

          


          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderBottomWidth:0}]}>
              <Text style={{fontSize:12}} >Mobile : </Text>
              <Text style={{fontSize:15}} >{this.state.data.phone}</Text>
          </View>

          
         
 
      </View> 

          : this.state.editMode == true ?                  // Edit Mode //
          <View style={[styles.fullProfileCard,{marginTop:deviceHeight/20,marginBottom:deviceHeight/20 }]}>
          
          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >First name :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlign:'right'}}
                placeholder=" First name"
                 defaultValue={this.state.first_name}
                placeholderTextColor={"#707070"}
                onChangeText={text => this.setState({first_name:text})}
                  />
          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Last name :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlign:'right'}}
                 placeholder=" Last name"      
                 defaultValue={this.state.last_name}
                placeholderTextColor={"#707070"}
                onChangeText={text => this.setState({last_name:text})}
   
                />
          </View>

          
{/* 
          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Password :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlign:'right'}}
                 defaultValue={this.state.password}
                placeholder=" Password"      
                secureTextEntry
                placeholderTextColor={"#707070"}
                onChangeText={text =>{text => this.validate_PS(text, "password")}}
   
                />
          </View>


          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Password Confirm :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlign:'right'}}
                 defaultValue={this.state.password_confirmation}
                placeholder=" Re-Password"      
                secureTextEntry
                placeholderTextColor={"#707070"}
                onChangeText={text => {text => this.validate_PSC(text, "password_confirm")}}
                />
          </View>
 */}


         
          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingRight:0}]}>
              {/* //<DatePicker  dateChange={this.handleDate} date={this.state.birth_date}/>   */}
              <RawDateTimePicker dateChange={this.handleDate} date={this.state.birth_date} />

          </View>

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>

              <Text style={{fontSize:12}} >GENDER :</Text>
              <TouchableOpacity onPress={()=>this.toggleModal()}>
                {this.state.gender?
                <Text style={{fontSize:16,color:"#000"}}> 
                {this.state.gender === "m" ? "Male " : 'Female '
                }</Text>
                :
                <Text style={{fontSize:16,color:"#000"}}>Select gender </Text>
                }
              </TouchableOpacity>
          </View>



          <Modal isVisible={this.state.isModalVisible} onBackdropPress={Keyboard.dismiss}>
                  <View style={{height:deviceHeight/6,backgroundColor:'black',width:deviceWidth/1.2,alignSelf:"center"
                  ,borderRadius:deviceWidth/10}} >
                    <View style={{
                      backgroundColor: "#fff",borderRadius:deviceWidth/20,flex:1,alignContent:'space-between'
                    }}>
                      <ListItem noIndent	 style={{ justifyContent: "space-between" }}>
                        <Text style={{fontSize:15,color:'#000'}}>Choose your gender ! </Text>
                        <Icon name="close" type="MaterialIcons" style={{ color: "#707070" }} onPress={() => this.toggleModal()} />
                      </ListItem>
                      <View style={{flexDirection:"row" ,alignItems:'center',flex:1 ,marginTop:deviceHeight/60,justifyContent:'center'}}>
                      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 5 }}>
                        <Button 
                          title="Hide modal" style={[{
                            justifyContent: "center", marginHorizontal: 2,
                            width: 100, height: 40, borderRadius: 5,backgroundColor: "#FF952B"
                          } ]} onPress={() => this.changeGender('m')} >
                          <Text style={{ color: "#fff" }}>Male</Text></Button>
                      </View>
                      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 5 }}>
                        <Button 
                          title="Hide modal" style={[{
                            justifyContent: "center", marginHorizontal: 2,
                            width: 100, height: 40, borderRadius: 5,backgroundColor: "#FF952B"
                          } ]} onPress={() => this.changeGender('f')} >
                          <Text style={{ color: "#fff" }}>Female</Text></Button>
                      </View>
                      </View>

                    </View>

                  </View>
                </Modal>


 
             <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}]}>
              <Text style={{fontSize:12}} >Country  :</Text>
              <Text style={{fontSize:15}} >Egypt </Text>

              </View>


        
 
              <Govrn gov={this.state.data.governrate.en_name} 
                     city ={this.state.data.city.en_name}
                     cityChange= {this.handleCity} 
                     governChange={this.handleGovrn}   />
          

            <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center'
            ,justifyContent:'space-between',alignContent:'center'}]}>

              <Text style={{fontSize:12}} >Adress :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlign:'right'}}
                placeholder=" Address"
                placeholderTextColor={"#707070"}
                onChangeText={text => this.setState({address:text})}
                defaultValue={this.state.address}      
                />
          </View>
 

          <View style={[styles.profileInfoTab,{flexDirection:'row',alignItems:'center'
           ,borderBottomWidth:0,justifyContent:'space-between'}]}>

              <Text style={{fontSize:12}} >Mobile :</Text>
              <Input style={{fontSize:15 ,height:20,alignSelf:'flex-end',textAlignVertical:'center',textAlign:'right'}}
                placeholder=" Phone number"
                keyboardType='phone-pad'
                placeholderTextColor={"#707070"}
                onChangeText={text => this.setState({phone:(text)})}
                value={this.state.phone?(`${this.state.phone}`):null}
                defaultValue={this.state.phone?(`${this.state.phone}`):null}

                /> 
          </View>

        

         
 
      </View>
          

          : null }

 
                        <View >
                        <TouchableOpacity
                         onPress={this.state.editMode == true ?() => this.Update()
                            :this.state.editMode == false 
                            ? () => this.setState({ editMode: true,avatar:this.state.avatar  })
                            : null }
                          style={[styles.CartButton,{marginBottom:10}]}>
                           <Text style={styles.CardButtonText}> {this.state.editMode == true? "Done" : "Edit Profile"}</Text>
                           </TouchableOpacity>
                         </View>

    
                    </Content>
                </Container>
                : <Loader />

        )
    }


//handlers
handleCity=async(data)=>
{
  data ? await this.setState({city_id:data})
  : await this.setState({city_id:this.state.data.city_id})
  //console.log(this.state.city_id);
}
handleGovrn=async(data)=>
{
  data ? await this.setState({governrate_id:data})
  : await this.setState({governrate_id:this.state.governrate_id});

}
onImgchange(image) {
  var self = this;

  self.setState({ avatar: image });
  //console.log(this.state.avatar);
}
handleDate=async(data)=>
{console.log("date is  changed in parent and  now is ",data)
  await this.setState({birth_date:data});

}
handleGender=async(data)=>
{
  await this.setState({gender:data});

}

 

    //validation
  validate_UNa = (text, type) => {
    if (type == 'firstname') {
      var self = this;
      self.setState({
        first_name: text
      });
      if (text.length < 2) {
        self.setState({ FirstNameError: true });
      }
      else {
        self.setState({  FirstNameError: false });

      }
    }
    if (type == 'lastname') {
      var self = this;
      self.setState({
        last_name: text
      });
      if (text.length < 2) {
        self.setState({ LastNameError: true });
      }
      else {
        self.setState({  LastNameError: false });

      }
    }
    if (type == 'address') {
      var self = this;
      self.setState({
        address: text
      });
      if (text.length < 3) {
        self.setState({ AddressError: true });
      }
      else {
        self.setState({  AddressError: false });

      }
    }
  }
  validate_PS = (text, type) => {

    var self = this;

    if ((type = "password")) {
      self.setState({
        password: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }
   
  validate_PSC = (text, type) => {
    var self = this;

    if ((type = "password_confirm")) {
      self.setState({
        password_confirmation: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }
  validate_PH = (text, type) => {
    var self = this;

    num = /^[0-9]+$ /
    if ((type = "phone")) {
      self.setState({
        phone: text
      });
      if ((text.length < 7) || (text.length > 12)) {
        self.setState({ MobileError: true });
      } else {
        self.setState({ MobileError: false });
      }
    }
  }
}









