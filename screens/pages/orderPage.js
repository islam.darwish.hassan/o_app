import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left,
  Body,
  Thumbnail
} from 'native-base';
import { API, APIV, Orders } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image, Dimensions } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Loader from "../Loader";
import StoreMan from "../../functions/storage";

export default class orderPage extends Component {
  //construction
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };
  state = { token: null, data: null, isReady: false }
  componentDidMount = async () => {
    const { navigation } = this.props;
    const Data = navigation.getParam('data', '');
    await this.setState({ data: Data });
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({ token: stored_token })
    await this.Get_Order();
    await this.setState({ isReady: true });

  }
  Get_Order = async () => {
    await axios
      .get(API + APIV + Orders + "/"+this.state.data.id, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
        this.setState({
          data: result.data.data.my_order
        })
        console.log("Order has been set");

      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });

  }

  render() {
    data=this.state.data;
    return (
      this.state.isReady ?
        <Container>
          <Header style={{ backgroundColor: "#FF952B", justifyContent: "space-between", alignItems: "center" }}>
            <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
              <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> {data.hash_code}</Text>
            </View>
          </Header>
          <Content>
            <List>
              <ListItem >

                <Body style={{ flex: 1 }}>
                  <View >
                    <Text numberOfLines={1} style={styles.TextSmalBlack}> HashCode: {data.hash_code}</Text>
                    <Text numberOfLines={1} note><Icon name="palette-swatch" type="MaterialCommunityIcons" style={{ fontSize: 16, color: "#FF952B" }} /> Status : {data.status_message}</Text>
                   <Text numberOfLines={2} note><Icon name="map-marker-radius" type="MaterialCommunityIcons" style={{ fontSize: 16, color: "#FF952B" }} /> Address: {data.address}</Text>
                  <Text numberOfLines={1} note><Icon name="phone" type="MaterialCommunityIcons" style={{ fontSize: 16, color: "#FF952B" }} /> Phone: {data.phone}</Text>
                  </View>
                 </Body>
              </ListItem>


              <ListItem noBorder  >
                <View style={{ flexDirection: "row", flex: 1, justifyContent: "space-between", alignItems: "center" }}>
                  <Text note >Products</Text>
                </View>
              </ListItem>
              {data.order_products.map((value, index) => {
                let product =value.product
                return (
                  <View key={index}>
                  <ListItem  onPress={() => this.props.navigation.navigate("Product", {
                    data: product
                  })}>
                    <Left style={{ flex: 0.3 }}>
                      <Thumbnail large resizeMethod='auto' resizeMode="contain"
                        source={{ uri: product.image }} >
                      </Thumbnail>
                    </Left >
                    <Body style={{ flex: 0.8 }}>
                      <View >
                        <Text numberOfLines={1} style={styles.TextList}>{product.name}</Text>
                        <Text numberOfLines={2} note>{product.description}</Text>
                      </View>
                    </Body>
                    {product.discount.has_discount?
                      <View style={styles.Offerbadge}><Text style={styles.TextSmallWhite}>{product.discount.value}</Text></View>
                      :null}
                  </ListItem>
                  <ListItem style={{justifyContent:"space-between" , paddingHorizontal:10}}>
                  <Text><Text note >Total Price: </Text>{value.total_price}</Text>
                  <Text><Text note >Quantity: </Text>{value.qty}</Text>
                  </ListItem>
                  </View>
                )
              })}

            </List>
          </Content>
        </Container>
        : <Loader />

    )
  }
}










