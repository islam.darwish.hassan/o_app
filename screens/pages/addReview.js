import React, { Component } from 'react'
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left, Body, Thumbnail
} from 'native-base';
import { API, APIV, } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image, Linking, Dimensions, ScrollView, FlatList, TextComponent ,TextInput} from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
 const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import StoreMan from "../../functions/storage";
//redux 
import { connect } from "react-redux";
import { addItem, deleteItem, setItems } from "../../store/actions/index";
import Loader from '../Loader'

import { Rating, AirbnbRating } from 'react-native-ratings';
 
 
export  default class AddReview extends Component {

    //construction
    constructor(props) {
      super(props);
      this.Storager = new StoreMan();
    };
  
  
    state = { data: null, isReady: false,product_id:'', product_rate:'',text_good:'',text_bad:'',review_text:''}
  
    componentDidMount = async () => {
        await this.setState({ isReady: false });

      const { navigation } = this.props;
      const Data = navigation.getParam('data', '');
      await this.setState({ product_id: Data });

      const stored_token = await this.Storager.get_item("USER_TOKEN");
      await this.setState({ token: stored_token })
       await this.setState({ isReady: true });

  
    }
  
    handleRate = async (data) => {
        await this.setState({ product_rate: data });
        console.log(this.state.product_rate);
      }
     

      
    post_Review = async (value) => {
        await this.setState({ isReady: false });
        
  
  
        var config={
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": " Bearer ".concat(this.state.token)
              }
        };
        var body ={
                rate:this.state.product_rate,
                review:this.state.review_text,
                 
             
        }
        await axios  
        .post ( API + APIV + Products + "/" + this.state.product_id + "/" + Reviews ,body,config)
        .then(result => {
          alert("Done 👌");
         this.props.navigation.goBack()
        })
        .catch(function (error) {
          console.log(error);
          if (error.response) {
            //error handling 
            alert(error.response.data.error);
           this.props.navigation.goBack()
          }
        })
        ;
        
    }
    
    
    render() {
   
      return (
        this.state.isReady ?
          <Container>
           <Header style={{backgroundColor:"#FF952B" }} androidStatusBarColor={'#fff'} iosBarStyle={Platform.OS=='ios'?'':'light-content'} >
                  <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                      <Icon onPress={() => this.props.navigation.goBack()} style={{ color: "#fff", paddingRight: 10, }} type="FontAwesome" name="angle-left" size={24} />
                    </TouchableOpacity>
                    <Text numberOfLines={2} ellipsizeMode='head' style={[ {
                      paddingHorizontal: 0.5, flex: 1
                      ,   fontSize: 16, color: "#fff"
                    }]}>Add a review</Text>
       
                  </View>
                    
                </Header>
            <Content>
            <View padder style={{flex:1}} >

            <Rating
                          
                          type='star'
                          startingValue={3}
                         
                          fractions={1}
                        ratingColor={'#FEC001'}
                          showRating
                          onFinishRating = {this.handleRate}
                          reviewSize={10}
                          ratingBackgroundColor='#c8c7c8'
                          ratingCount={5}
                          imageSize={28}
                          style={{ padding: 2, margin: 0,alignSelf:'center'}}
                          
                        />              


                        
             <View  style={{borderBottomWidth:1,borderColor:'#ededed',marginVertical:5}} >
                <Text style={styles.textBigGrey}>Tell us your opinion</Text>
                <View style ={{height:deviceHeight/5,marginVertical:deviceHeight/35,width:deviceWidth/1.05,
                  backgroundColor:'white',alignSelf:"center",borderWidth:1,padding:5}}>
                      <TextInput 
                         
                          multiline={true}
                          onChangeText={text => this.setState({review_text: text})}
                          value={this.state.review_text}
                          numberOfLines={3}
                          maxLength={300} 
                          placeholder=" Tell us your opinion ?  "
                          
                          style={{fontSize:15}}
                          textAlignVertical={'top'}
                        /> 
                </View>        
                <TouchableOpacity style={[styles.CartButton,{marginBottom:20}]} onPress={()=>this.post_Review()} >
              <Text style={styles.CardButtonText}>Post Reivew</Text>
              </TouchableOpacity>
              </View>
              </View>
            </Content>
          </Container> : <Loader />
      )
    }
  }

  
  
   
  