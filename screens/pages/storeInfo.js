import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem,List,Left,
  Body, 
  Thumbnail} from 'native-base';
import { API, APIV, Store } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image ,Linking} from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from"../Loader";
import StoreMan from "../../functions/storage";
import MapView from 'react-native-maps';

export default class StoreInfo extends Component {
    state={data: null ,isReady:false,showMap:true}
    componentDidMount=async()=>{
        const { navigation } = this.props;
         const Data = navigation.getParam('data', '');
        await this.setState({data:Data});
        await this.setState({isReady:true});

    }
  render() {
      data=this.state.data;
    return (
      this.state.isReady ? 
      <Container>
        <Header style={{ backgroundColor: "#FF952B" ,justifyContent:"space-between" ,alignItems:"center"}}>
          <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
            <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> {this.state.data.name}</Text>
          </View>
           
        </Header>
        <Content  >
        <Image style={styles.ImageHeader} source={{uri:this.state.data.image}}/>      
        <List>
        <ListItem noIndent  >
        <Text note >Basic Info</Text>
         </ListItem>
         <View padder>
            <TouchableOpacity  style={styles.InfoList}>
                <Icon style={{ color: "#FF952B", }} type="MaterialIcons" name="location-on" size={15} />
                <Text note> Address: {data.address}</Text></TouchableOpacity>
            <TouchableOpacity style={styles.InfoList} onPress={()=>Linking.openURL('mailto: '+data.contact_email) }>
                <Icon style={{ color: "#FF952B", }} type="MaterialIcons" name="email" size={15} />
                <Text note> Email: {data.contact_email}</Text></TouchableOpacity>
            <TouchableOpacity style={styles.InfoList} onPress={()=>Linking.openURL('tel: '+data.phone) } >
            <Icon style={{ color: "#FF952B", }} type="MaterialIcons" name="phone" size={15} />
            <Text note> Phone: {data.phone}</Text></TouchableOpacity>
            <View style={styles.InfoList}>
              <Icon name="access-time" type="MaterialIcons"  style={{   color:"#FF952B"}} size={15}/>
              <Text note> Open at: {data.open_at}</Text> 
            </View>
            <View style={styles.InfoList}>
              <Icon name="access-time" type="MaterialIcons"  style={{   color:"#FF952B"}} size={15}/>
              <Text note> Close at : {data.close_at}</Text> 
            </View>
         </View>
         </List>
         {this.state.showMap ? <MapView style={{ alignSelf: 'stretch', height: 200 }}
         initialRegion={{
           latitude: this.state.data.latitude,
           longitude: this.state.data.longitude,
           latitudeDelta: 0.0922,
           longitudeDelta: 0.0421,
         }}
       >
         <MapView.Marker 
         
           coordinate={{
             latitude: this.state.data.latitude,
             longitude: this.state.data.longitude,
           }}
           title={"You"}
           description={"Your location"}
         />
       </MapView> : null}
          <Text style={{backgroundColor:'#fff'}}></Text>
        </Content>
      </Container>
      : <Loader/>
  
    )
  }
}
