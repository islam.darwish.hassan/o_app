import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer } from 'native-base';
import { API, APIV, Categories } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from"../Loader";
import StoreMan from "../../functions/storage";

export default class CategoriesPage extends Component {
  //construction
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
};
  state = { token:null, data: null, isReady: false }
  componentDidMount = async () => {
    await this.setState({ isReady: false });
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({token:stored_token})
    await this.Get_Categories();
    await this.setState({ isReady: true });

  }
  Get_Categories = async () => {
    await axios
      .get(API + APIV + Categories, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
        this.setState({
          data: result.data.data.categories
        })
        console.log("Categories have been set");

      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });

  }

  render() {
    return (
      this.state.isReady && this.state.data ?
        <Container>
          <Header style={{ backgroundColor: "#FF952B" }}>
            <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
              <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}><Icon style={{ color: "#fff", }} type="MaterialCommunityIcons" name="reorder-horizontal" size={20} /></TouchableOpacity>
              <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> Categories</Text>
            </View>
          </Header>
          <Content>
            {this.state.data.map((value, index) => {
              console.log(value.image)
              return (
                <TouchableWithoutFeedback key={index} style={styles.Card}
                onPress={()=>this.props.navigation.navigate("Stores",{data:value})}>
                  <Image style={styles.ImageCard}
                    source={{ uri:value.image }} ></Image>
                 <View style={styles.FooterCard}>
                    <Text style={styles.TextSmallWhite}>{value.name}</Text>
                    </View>
                </TouchableWithoutFeedback>
              )
            })}
          </Content>
        </Container>
        : <Loader/>

    )
  }
}
