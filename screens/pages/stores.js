import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem,List,Left,
  Body, 
  Thumbnail} from 'native-base';
import { API, APIV, Categories } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image ,Dimensions} from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Loader from"../Loader";
import StoreMan from "../../functions/storage";

export default class StoresPage extends Component {
    //construction
    constructor(props) {
      super(props);
      this.Storager = new StoreMan();
  };
  
    state={token:null,data: null ,isReady:false}
    componentDidMount=async()=>{
      const { navigation } = this.props;
      const Data = navigation.getParam('data', '');
      const stored_token = await this.Storager.get_item("USER_TOKEN");
      await this.setState({token:stored_token})
      await this.setState({data:Data});
      await this.Get_Stores();
      await this.setState({isReady:true});

    }
    Get_Stores = async () => {
      await axios
        .get(API + APIV + Categories +"/"+this.state.data.id +"/stores", {
          headers: {
            "Content-Type": "application/json", 
            "Accept": "application/json",
            "Authorization": " Bearer " + this.state.token
          },
        })
        .then(result => {
          this.setState({
            data:result.data.data.stores
          })
          console.log("Stores have been set");
  
        })
        .catch(function (error) {
          console.log(error);
          if (error.response) {
            //error handling
            alert(error.response.data.error);
          }
        });
  
    }
  
  render() {
    return(
    this.state.isReady && this.state.data ?
    <Container>
      <Header style={{ backgroundColor: "#FF952B" }}>
        <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
        <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
        <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> Stores</Text>
        </View>
      </Header>
      <Content>
      <List>
        {this.state.data.map((value, index) => {
          return (
            <ListItem  key={index}onPress={()=>this.props.navigation.navigate("Store",{data:value})}>
            <Left style={{flex:0.3}}>
            <Thumbnail large resizeMethod='auto' resizeMode="contain"
            source={{ uri:value.image }} >
            </Thumbnail>
            </Left >
              <Body style={{flex:0.8}}>
                <View >
                  <Text numberOfLines={1} style={styles.TextList}>{value.name}</Text>
                  <Text numberOfLines={2} note>{value.description}</Text>
                      <Text numberOfLines={1} note><Icon name="clock-outline" type="MaterialCommunityIcons" style={{fontSize:16 ,color:"#808080"}}/> Opened : {value.open_at}</Text>
                      <Text numberOfLines={1} note><Icon name="clock-outline" type="MaterialCommunityIcons" style={{fontSize:16 ,color:"#808080"}}/> Closed : {value.close_at}</Text>
                </View>
              </Body>
            </ListItem>

          )
        })}
        </List>
      </Content>
    </Container>
    : <Loader/>
    )
  }
}
