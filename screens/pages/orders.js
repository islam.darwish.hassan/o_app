import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem,List,Left,
  Body, 
  Thumbnail} from 'native-base';
import { API, APIV, Orders } from "../../functions/config";
import axios from "axios";
import styles from "../style";
import { Image ,Dimensions} from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Loader from"../Loader";
import StoreMan from "../../functions/storage";
export default class OrdersPage extends Component {
//construction
constructor(props) {
    super(props);
    this.Storager = new StoreMan();
};
state={token:null,data: null ,isReady:false}
componentDidMount=async()=>{
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({token:stored_token})
    await this.Get_Orders();
    await this.setState({isReady:true});

    }
    Get_Orders = async () => {
    await axios
        .get(API + APIV + Orders +"?page=1&status=0", {
        headers: {
            "Content-Type": "application/json", 
            "Accept": "application/json",
            "Authorization": " Bearer " + this.state.token
        },
        })
        .then(result => {
        this.setState({
            data:result.data.data.my_orders
        })
        console.log("Orders have been set");

        })
        .catch(function (error) {
        console.log(error);
        if (error.response) {
            //error handling
            alert(error.response.data.error);
        }
        });

    }
  
    render() {
        return(
        this.state.isReady && this.state.data ?
        <Container>
          <Header style={{ backgroundColor: "#FF952B" }}>
            <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon style={{ color: "#fff", }} type="Ionicons" name="md-arrow-round-back" size={20} /></TouchableOpacity>
            <Text numberOfLines={2} ellipsizeMode='head' style={[styles.HeaderTitle, { paddingHorizontal: 0.5, flex: 1, fontSize: 18, color: "white" }]}> My Orders</Text>
            </View>
          </Header>
          <Content>
          <List>
            {this.state.data.map((value, index) => {
              return (
                <ListItem  key={index}onPress={()=>this.props.navigation.navigate("Order",{data:value})}>
                  <Body style={{flex:1}}>
                    <View padder>
                    <Text numberOfLines={1} style={styles.TextSmalBlack}> HashCode: {value.hash_code}</Text>
                    <Text numberOfLines={1} note><Icon name="palette-swatch" type="MaterialCommunityIcons" style={{fontSize:16 ,color:"#FF952B"}}/> Status : {value.status_message}</Text>
                     <Text numberOfLines={2}  note><Icon name="map-marker-radius" type="MaterialCommunityIcons" style={{fontSize:16 ,color:"#FF952B"}}/> Address: {value.address}</Text>
                    <Text numberOfLines={1} note><Icon name="phone" type="MaterialCommunityIcons" style={{fontSize:16 ,color:"#FF952B"}}/> Phone: {value.phone}</Text>
                    </View>

                    </Body>

                 </ListItem>
              )
            })}
            </List>
          </Content>
        </Container>
        : <Loader/>
        )
      }
    }
    