import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer } from 'native-base';
import StoreMan from "../functions/storage";
import axios from "axios";
import { API, Login, APIV,FAKE_TOKEN } from "../functions/config";
import {TouchableOpacity,Dimensions,Image} from"react-native";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;
const logo =require("../assets/logomin.png")
import Loader from "./Loader"
import { StackActions ,NavigationActions} from 'react-navigation';

export default class login extends Component {
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  }

  state = {
    data: 'no data',
    email: '',
    password: '',
    //error view checker
    EmailError: false,
    PasswordError: false,
    isReady: false
  }
  //validation 
  validate_EM = (text, type) => {
    var self = this;
    if (type == "Email") {
      //set variable from input field
      self.setState({
        email: text
      });
      //check if there any errors 
      ;
      if (text.length < 5 || !text.includes("@")) {
        self.setState({ EmailError: true });
      } else {
        self.setState({ EmailError: false });

      }
    }
  }
  validate_PS = (text, type) => {
    var self = this;

    if ((type = "password")) {
      self.setState({
        password: text
      });
      if (text.length < 5) {
        self.setState({ PasswordError: true });
      } else {
        self.setState({ PasswordError: false });

      }
    }
  };
  Login = async () => {
    this.setState({ isReady: false });
    //here you are login request 
    await axios
      .post(API + APIV + Login, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        email: this.state.email,
        password: this.state.password
      })
      .then(result => {
        console.log("User has been set");
        
        this.Storager.set_item("USER_TOKEN", result.data.data.user.token);
        this.Storager.set_item("USER_EMAIL", result.data.data.user.email);
        this.Storager.set_item("USER_CITY", result.data.data.user.city.en_name);
        this.Storager.set_item("USER_GOVRN", result.data.data.user.governrate.en_name);
        //this.Storager.set_item("USER_AVATAR",result.data.data.avatar);
        this.Storager.set_item("USER_IDKEY", result.data.data.user.id.toString());
        this.navigateToLogin();


      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({ isReady: true });

  }

  navigateToLogin() {
    this.props.navigation.dispatch(
      StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
      })
      )
    this.props.navigation.dispatch(resetAction);
  }








  componentDidMount= async() =>{
    let token = await this.Storager.get_item("USER_TOKEN");
    if(token){this.props.navigation.navigate("Drawer");}
    this.setState({ isReady: true });

  }
  render() {
    if (!this.state.isReady) {
      return <Loader/>;
    } else {

      return (
        <Container style={{backgroundColor:"#FF952B" ,height:deviceHeight}}>
        <Content>
        <View style={{height:deviceHeight/3}}>
        <Image source={logo} style={{width:deviceWidth-20 ,top:-deviceHeight/5 ,resizeMode:"contain"}}/>
        </View>
          <View  style={{height:deviceHeight/3}} padder>
            <Form >
              <View padder>
              <View style={{ flexDirection: "row",marginTop:10 }}>
              <View style={{ flex: 0.3 }}
              >
                <View 
                style={{ backgroundColor: "white", width: 50, height: 50, borderRadius: 50/2 ,
                justifyContent:"center",alignItems:"center",
                alignSelf:"center" }}>
                <View style={{backgroundColor: "#FF952B", width: 35, height: 35, borderRadius: 35/2}}/>
                </View>
              </View>
              <Item
                style={{ flex: 0.7 ,backgroundColor:"white" ,borderRadius:50 ,heigth:100 ,paddingHorizontal:10}}
                error={this.state.EmailError}
                inlineLabel
                >
                <Input
                placeholder="Email Address"
                  onChangeText={text => this.validate_EM(text, "Email")}
                  value={this.state.email}
                />
              </Item>
            </View>

                <View style={{ flexDirection: "row",marginTop:10 }}>
                  <Item
                    style={{ flex: 0.7 ,backgroundColor:"white" ,borderRadius:50 ,heigth:100 ,paddingHorizontal:10}}
                    error={this.state.PasswordError}
                    inlineLabel
                    >
                    <Input
                    placeholder="Password"
                      secureTextEntry
                      onChangeText={text => this.validate_PS(text, "password")}
                      value={this.state.password}
                    />
                  </Item>
                  <View style={{ flex: 0.3 }}
                  >
                    <TouchableOpacity 
                    onPress={() => this.Login()}
                    style={{ backgroundColor: "white", width: 50, height: 50, borderRadius: 50 ,
                    justifyContent:"center",alignItems:"center",
                    alignSelf:"center" }}>
                    <Icon name="play-arrow" type="MaterialIcons" style={{color:"#FF952B" ,fontSize:22}}/>
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity onPress={()=>console.log("pressed")} style={{marginTop:10 ,paddingHorizontal:15}}>
                <Text style={{fontWeight:'bold'}}>Forget Password !</Text>
                </TouchableOpacity>
              </View>
            </Form>
          </View>
          <View style={{height:deviceHeight/3}}>
           <TouchableOpacity
           onPress={()=>this.props.navigation.navigate("PhoneVerify")}
           style={{ 
            width: deviceWidth/1.2 , height:50, backgroundColor:"white" ,borderRadius:50 , alignSelf:"center"
           ,paddingHorizontal:10,justifyContent:"center" ,alignItems:"center"}}>
           <Text style={{color:"#FF952B", fontSize:24}}>Sign Up</Text>
           </TouchableOpacity>
          </View>
          </Content>
          </Container>
      )
    }
  }

}
