import React, { Component } from "react";
import { TouchableOpacity, Linking } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Title,
  Right, Header, Footer,Badge, View, Body
} from "native-base";
import styles from "./style";
import stylesheet from "../style";
import axios from "axios";

import { API, APIV, Categories  } from "../../functions/config";

import StoreMan from "../../functions/storage";
class SideBar extends Component {
  state = { categories: [] ,email:null}
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
    this.Storager = new StoreMan();

  }
  componentDidMount = async () => {
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    if(!stored_token){
      this.props.navigation.replace("Login")
    }
    await this.setState({token:stored_token})
    const Email = await this.Storager.get_item("USER_EMAIL");
    await this.Get_Categories();
    await this.setState({ email: Email })
  }
  Get_Categories = async () => {
    await axios
      .get(API + APIV + Categories, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token

        },
      })
      .then(result => {
        this.setState({
          categories: result.data.data.categories
        })
        console.log("Categories have been set");

      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });

  }
  flush = async () => {
    await this.Storager.remove_all();
    this.props.navigation.navigate("Login")
  }
  render() {
    return (
      <Container style={{ backgroundColor: "#ededed" }} >
        <Content
          bounces={false}
          style={{ flex: 1, top: -1, }}
        >
          <View style={{
            flexDirection: "row", flex: 1, justifyContent: "space-between",
            padding: 10, backgroundColor: "#FF952B"
          }}>
            <Title style={{ color: "white" }} >  O App</Title>
            <Icon type="MaterialCommunityIcons" name="window-close" style={{ color: "white" }} size={32} onPress={() => this.props.navigation.closeDrawer()} />
          </View>
          <View style={stylesheet.Line}></View>
          <View >
          {this.state.email?
            <ListItem noBorder style={{ flex: 1, alignItems: "center", alignSelf: "flex-start", paddingTop: 10, justifyContent: "center" }}
             avatar onPress={() => this.props.navigation.navigate("Profile")}  >
               <View >
            <View style={{ flex: 0.2 ,alignSelf :'center'}}>
              <Icon name="account" type="MaterialCommunityIcons" style={{ fontSize: 50, color: "#FF952B" }} />
            </View>
            <View style={{ flex: 0.7 }}>
              <Text style={{ fontWeight: "bold", marginRight: 5, color: "#111" }}>
                {this.state.email}
                  </Text>
            </View>
            </View>
          </ListItem>
            :
            <ListItem noBorder style={{ flex: 1, alignItems: "center", alignSelf: "flex-start", paddingTop: 10, justifyContent: "center" }} avatar onPress={() => this.props.navigation.navigate("Login")}  >
              <View style={{ flex: 0.2 }}>
                <Icon name="account" type="MaterialCommunityIcons" style={{ fontSize: 35, color: "#FF952B" }} />
              </View>
              <View style={{ flex: 0.7 }}>
                <Text style={{ fontWeight: "bold", marginRight: 5, color: "#111" }}>
                  Username
                    </Text>
                <Text note >Sign in to your account</Text>
              </View>
            </ListItem>
          }
          </View>
          <View style={stylesheet.Line}></View>
          {this.state.email?
            <ListItem style={{ justifyContent: "space-between", alignItems: "center" }} onPress={() => this.props.navigation.navigate("Orders")} >
            <Text note >MyOrders</Text><Icon name="ios-more" type="Ionicons" />
          </ListItem>
          :null}
          <View style={stylesheet.Line}></View>
          <ListItem style={{ justifyContent: "space-between", alignItems: "center" }} onPress={() => this.props.navigation.navigate("Categories")}  >
            <Text note >Categories</Text><Icon name="ios-more" type="Ionicons" />
          </ListItem>
          {this.state.categories ?
            this.state.categories.map((value, index) => {
              return (
                <ListItem key={index} noBorder onPress={() => this.props.navigation.navigate("Stores", { data: value })}  >
                  <Text note >{value.name}</Text>
                </ListItem>
              )
            }) : null}
          <View style={stylesheet.Line}></View>
          {this.state.email?
          <ListItem style={{ justifyContent: "space-between", alignItems: "center" }} onPress={() => this.flush()} >
          <Text note > Logout</Text><Icon name="md-log-out" type="Ionicons" />
        </ListItem>
        :null}
        </Content>
      </Container>
    );
  }
}

export default SideBar;
