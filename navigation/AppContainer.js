import React, { Component } from "react";
import { createStackNavigator } from 'react-navigation-stack';
import Welcome from "../screens/welcome";
import Login from "../screens/login";
import SignUp from "../screens/signup";
import Home from "../screens/hometabs";
import Store from "../screens/pages/store.js"
import { createDrawerNavigator } from 'react-navigation-drawer';
import SideBar from "../screens/Drawer/index";
import Categories from "../screens/pages/categories";
import Stores from "../screens/pages/stores";
import Product from "../screens/pages/product";
import Profile from "../screens/pages/profile.js";
import Dev from "../screens/underdeveloper";
import StoreInfo from"../screens/pages/storeInfo.js";
import search from"../screens/Tabs/search";
import Orders from"../screens/pages/orders";
import Order from"../screens/pages/orderPage";
import addReview from "../screens/pages/addReview"
import Checkout from"../screens/pages/checkout";
import imagePage from "../components/imagePage";;
import dis from "../screens/pages/discountSearch";
import searchInaStore  from "../screens/pages/searchInStore";
import phoneVerify from "../screens/phone_verify"



const MainStack = createStackNavigator({

 Home:{screen:Home},
 Profile: { screen: Profile},
 Store: { screen: Store },
 StoreInfo:{screen:StoreInfo},
 Stores: { screen: Stores },
 Dev:{screen:Dev},
 Search:{screen:search},
 Product:{screen:Product},
 AddReview:{screen:addReview},
 Order:{screen:Order},
 Checkout:{screen:Checkout},
 ImagePage:{screen:imagePage},
 Dis:{screen:dis},
 SearchInStore:{screen:searchInaStore},
 
}, { 
  initialRouteName: 'Home', 
  headerMode: "none"

});

 const Drawer = createDrawerNavigator(
  {
    Home: { screen: MainStack},
    Categories: { screen: Categories },
    Orders:{screen:Orders},
    ImagePage:{screen:imagePage},
  },
  {
    initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    drawerPosition: "left",
    contentComponent: props => <SideBar {...props} />
  }
);

const LoginStack = createDrawerNavigator({
  Login:{screen:Login},
  SignUp:{screen:SignUp},
  PhoneVerify:{screen:phoneVerify},
  Drawer: { screen: Drawer },
  
}, {
  initialRouteName: "Login",
  headerMode: "none"

});


export default createStackNavigator({
  Welcome: { screen: Welcome },
  LoginStack:{screen:LoginStack},
  Drawer:{screen:Drawer},



}, {
  initialRouteName: 'LoginStack',
  headerMode: "none"

});